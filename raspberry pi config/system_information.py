import sys
import shlex
import string
import time
import requests
import json
import os, subprocess
import psutil

from uuid import getnode as get_mac

#run a shell command
def run_shell_commands(cmds):
    # split the commands
    cmds = cmds.split("|")
    cmds = list(map(shlex.split,cmds))

    # run the commands
    stdout_old = None
    stderr_old = None
    p = []
    for cmd in cmds:
        #logging.info('%s' % (cmd,))
        p.append(subprocess.Popen(cmd,stdin=stdout_old,stdout=subprocess.PIPE,stderr=subprocess.PIPE))
        stdout_old = p[-1].stdout
        stderr_old = p[-1].stderr
    return p[-1]

#get CPU as (percent)
def get_cpu():
    try:
        cpu = psutil.cpu_percent(interval=1)
        return cpu
    except:
        return -1

#get RAM as (used, free, buff/cache) in MB
# sum -> total
def get_ram():
    try:
        ramArr = psutil.virtual_memory()
        return (ramArr.total >> 20, ramArr.available >> 20, ramArr.used >> 20, ramArr.free >> 20)
    except:
        return -1

#get disk space (total, used, free) in MB
def get_disk_space():
    try:
        disk_space = psutil.disk_usage('/')
        return (disk_space.total >> 20, disk_space.used >> 20, disk_space.free >> 20)
    except:
        return -1

#up-time (time in seconds since last boot)
def get_up_time():
	try:
		with open('/proc/uptime', 'r') as f:
		    uptime_seconds = float(f.readline().split()[0])
	    	return uptime_seconds
	except:
		return -1

#connection speed in bytes/sec (sending, receiving)
def get_network_speed():
	try:
		network = psutil.net_io_counters(pernic=False)
		bytes_s1 = network.bytes_sent
		bytes_r1 = network.bytes_recv
		time.sleep(1)
		network = psutil.net_io_counters(pernic=False)
		return ((network.bytes_sent - bytes_s1), (network.bytes_recv - bytes_r1));
	except:
		return -1

# Returns the temperature in degrees C
def get_temperature():
    try:
        s = subprocess.check_output(["/opt/vc/bin/vcgencmd","measure_temp"])
        return float(s.split('=')[1][:-3])
    except:
        return 0

# Returns the current IP address
def get_ipaddress():
    arg='ip route list'
    p=subprocess.Popen(arg,shell=True,stdout=subprocess.PIPE)
    data = p.communicate()
    split_data = data[0].split()
    ipaddr = split_data[split_data.index('src')+1]
    return ipaddr

# get top 5 processes
def get_top_5_process():
	try:
		cmd1 = 'ps aux | sort -rk 3,3 | head -n 6'
		p = run_shell_commands(cmd1)
		out = p.communicate()


		stringArr = out[0].split('\n');

		top5Proc = {}

		for index in xrange(1,6):
			proc = stringArr[index].split()

			if len(proc) > 0:
				procTemp = {}
				procTemp['process_name'] = proc[10]
				procTemp['PID'] = proc[1]
				procTemp['CPU_usage'] = proc[2]
				procTemp['Mem_usage'] = proc[3]

				top5Proc['process_' + str(index)] = procTemp

		return top5Proc

	except:
		e = sys.exc_info()[0]
		return -1

# ping router and get results
def get_ping_router_results():
    # get router IP address
    #response = requests.get('http://opsdev.sence.io/backend/api-functions/whatsMyIP.php')
    #routerIP = response.text

    try:
	cmd = "ip route show | grep -i 'default via'| awk '{print $3}'";
	p = run_shell_commands(cmd)
        out = p.communicate()
        stringArr = out[0].split('\n')
	routerIP = stringArr[0]	
	
    	cmd1 = "ping -c 5 "+routerIP+" | tail -1| awk '{print $4}' | cut -d '/' -f 2";
    	p1 = run_shell_commands(cmd1)
    	out1 = p1.communicate()
        stringArr1 = out1[0].split('\n');

        return stringArr1[0]
    except:
    	e = sys.exc_info()[0]
	return -1



# get this sensor's mac address (eth0)
def get_mac_add():
	#Get the mac address in 48-bit integer
	mac = get_mac()

	#Translate the raw mac address to a readable format
	hex_mac = ':'.join(("%012X" % mac )[i:i+2] for i in range(0, 12, 2))
	return hex_mac



output = {}

output['MAC'] = str(get_mac_add())

output['CPU'] = float(get_cpu())

output['RAM_Total'] = int(get_ram()[0])
output['RAM_Available'] = int(get_ram()[1])
output['RAM_Used'] = int(get_ram()[2])
output['RAM_Free'] = int(get_ram()[3])

output['Disk_Space_Total'] = int(get_disk_space()[0])
output['Disk_Space_Used'] = int(get_disk_space()[1])
output['Disk_Space_Free'] = int(get_disk_space()[2])

output['Uptime'] = float(get_up_time())
output['IP Address'] = str(get_ipaddress())

output['Network_Upload_Speed'] = int(get_network_speed()[0])
output['Network_Download_Speed'] = int(get_network_speed()[1])

output['Temperature'] = float(get_temperature())

output['top_process'] = get_top_5_process()

router_latency = get_ping_router_results()
if router_latency == -1:
	output['router_latency'] = -1
else:
	output['router_latency'] = float(get_ping_router_results())



# make POST request
url = 'http://localhost:8888/mongo.test'
headers = {'content-type': 'application/json', 'Accept-Charset':'UTF-8'}
r = requests.post(url, data=json.dumps(output, ensure_ascii=False), headers=headers)

exit
