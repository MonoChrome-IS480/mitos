#!/bin/bash

#get router ip
#routerIP="$(curl -s http://opsdev.sence.io/backend/api-functions/whatsMyIP.php)"
routerIP="$(ip route show | grep -i 'default via'| awk '{print $3}')"

pingReport="$(ping -c 10 ${routerIP} | tail -1| awk '{print $4}' | cut -d '/' -f 2)"

#get MAC address
#read MAC </sys/class/net/eth0/address
#MAC=$(echo $MAC | tr 'a-z' 'A-Z')

echo $pingReport
