#!/bin/bash

#get mac address
read MAC </sys/class/net/eth0/address

#convert to upper case
MAC=$(echo $MAC | tr 'a-z' 'A-Z')

#post to fluentd
#curl -H "Content-Type: application/json" -X POST -d '{"MAC":"'"$MAC"'"}' http://localhost:8888/uptime.mongo
curl --data "MAC=$MAC" http://opsdev.sence.io/backend/ping/

