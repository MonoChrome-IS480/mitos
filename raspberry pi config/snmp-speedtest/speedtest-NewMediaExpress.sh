#!/bin/bash

#run speed test
speedTestReport="$(/home/pi/speedtest-cli --server 367)"

#get upload speed
upload="$(grep Upload <<< "${speedTestReport}")"
IFS=' ' read -r -a upload_array <<< "$upload"
upSpeed="${upload_array[1]}"

#get download speed
download="$(grep Download <<< "${speedTestReport}")"
IFS=' ' read -r -a download_array <<< "$download"
downSpeed="${download_array[1]}"

#get MAC address
read MAC </sys/class/net/eth0/address
MAC=$(echo $MAC | tr 'a-z' 'A-Z')

#get current interval
interval=$(cut -c1-10 /etc/cron.d/speedtest)

#send output to fluentd
#curl -H "Content-Type: application/json" -X POST -d $(printf '{"MAC":"%s","upload":%s,"download":%s}' "$MAC" "$upSpeed" "$downSpeed") http://localhost:8888/speedtest.mongo
curl --data "MAC=$MAC&upload=$upSpeed&download=$downSpeed&interval=$interval" http://opsdev.sence.io/backend/ping/speedtest.php
