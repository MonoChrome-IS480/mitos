#!/bin/bash

#==========SPEED TEST==========
#run speed test
speedTestReport="$(/home/pi/speedtest-cli --server 367)"

#get upload speed
upload="$(grep Upload <<< "${speedTestReport}")"
IFS=' ' read -r -a upload_array <<< "$upload"
upSpeed="${upload_array[1]}"

#get download speed
download="$(grep Download <<< "${speedTestReport}")"
IFS=' ' read -r -a download_array <<< "$download"
downSpeed="${download_array[1]}"

#get MAC address
read MAC </sys/class/net/eth0/address
MAC=$(echo $MAC | tr 'a-z' 'A-Z')

#get current interval
interval=$(cut -c1-10 /etc/cron.d/snmp-speedtest)

#==========SNMPGET==========

routerIP="$(ip route show | grep -i 'default via'| awk '{print $3 }')"

model="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.1.1.0 | grep .1.3.6.1.2.1.1.1.0 | awk '{print $4, $5}')"

name="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.1.5.0 | grep .1.3.6.1.2.1.1.5.0 | awk '{print $4}')"

uptime="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.1.3.0 | grep .1.3.6.1.2.1.1.3.0 | awk '{print $4}' | sed 's/.$//; s/^.//')"

storageTotal="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.5.131073 | grep .1.3.6.1.2.1.25.2.3.1.5.131073 | awk '{print $4}')"
storageUsed="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.6.131073 | grep .1.3.6.1.2.1.25.2.3.1.6.131073 | awk '{print $4}')"

memoryTotal="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.5.65536 | grep .1.3.6.1.2.1.25.2.3.1.5.65536 | awk '{print $4}')"
memoryUsed="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.6.65536 | grep .1.3.6.1.2.1.25.2.3.1.6.65536 | awk '{print $4}')"

cpuFreq="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.4.1.14988.1.1.3.14.0 | grep .1.3.6.1.4.1.14988.1.1.3.14.0 | awk '{print $4}')"

processorTemp="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.4.1.14988.1.1.3.11.0 | grep .1.3.6.1.4.1.14988.1.1.3.11.0 | awk '{print $4}')"

cpuLoad="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.3.3.1.2.1 | grep .1.3.6.1.2.1.25.3.3.1.2.1 | awk '{print $4}')"

#===========================
#send results to server
curl --data "MAC=$MAC&upload=$upSpeed&download=$downSpeed&interval=$interval&model=$model&name=$name&uptime=$uptime&storageTotal=$storageTotal&storageUsed=$storageUsed&memoryTotal=$memoryTotal&memoryUsed=$memoryUsed&cpuFreq=$cpuFreq&cpuLoad=$cpuLoad&processorTemp=$processorTemp" http://opsdev.sence.io/backend/ping/snmp-speedtest.php
