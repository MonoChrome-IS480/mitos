#!/bin/bash

routerIP="$(ip route show | grep -i 'default via'| awk '{print $3 }')"

uptime="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.1.3.0 | grep .1.3.6.1.2.1.1.3.0 | awk '{print $4}' | sed 's/.$//; s/^.//')"

storageTotal="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.5.131073 | grep .1.3.6.1.2.1.25.2.3.1.5.131073 | awk '{print $4}')"
storageUsed="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.6.131073 | grep .1.3.6.1.2.1.25.2.3.1.6.131073 | awk '{print $4}')"

memoryTotal="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.5.65536 | grep .1.3.6.1.2.1.25.2.3.1.5.65536 | awk '{print $4}')"
memoryUsed="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.2.3.1.6.65536 | grep .1.3.6.1.2.1.25.2.3.1.6.65536 | awk '{print $4}')"

cpuFreq="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.4.1.14988.1.1.3.14.0 | grep .1.3.6.1.4.1.14988.1.1.3.14.0 | awk '{print $4}')"

processorTemp="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.4.1.14988.1.1.3.11.0 | grep .1.3.6.1.4.1.14988.1.1.3.11.0 | awk '{print $4}')"

cpuLoad="$(/usr/bin/snmpget -On -v 2c -c public $routerIP OID .1.3.6.1.2.1.25.3.3.1.2.1 | grep .1.3.6.1.2.1.25.3.3.1.2.1 | awk '{print $4}')"


echo "{"

echo "\"uptime\": $uptime"

echo "\"storageTotal\": $storageTotal"
echo "\"storageused\": $storageUsed"

echo "\"memoryTotal\": $memoryTotal"
echo "\"memoryUsed\": $memoryUsed"

echo "\"cpuFreq\": $cpuFreq"
echo "\"cpuLoad\": $cpuLoad"

echo "\"processorTemp\": $processorTemp"

echo "}"
