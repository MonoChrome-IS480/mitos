<?php

require 'vendor/autoload.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");
//$sensors = $client->fluent->sensor_matrix;

function getDowntimeEvents($month) {
    global $client;
	$sensors = $client->fluent->sensor_matrix;

	$result = array();

	$all_sensors = $sensors->find([],[
        'projection' => ['building' => 1]
    ])->toArray();

	foreach ($all_sensors as $sensor) {
        if (!in_array($sensor['building'], $result)) {
            array_push($result, $sensor['building']);
        }
	}

	return $result;
}

function insertNewDowntimeEvent($mac_addresses, $type, $startDate, $endDate) {
    global $client;
    $downtime_events = $client->fluent->downtime_events;

    $failed = array();

    foreach ($mac_addresses as $mac_address) {
        //check for repeated
        $existingRecord = $downtime_events->findOne([
            'MAC' => $mac_address,
            'type' => $type,
            'startDate' => $startDate,
            'endDate' => $endDate
        ]);

        if ($existingRecord !== null) {
            array_push($failed, array(
                $mac_address => "record already exists"
            ));
        } else {
            // insert new downtime event
            $insertResult = $downtime_events->insertOne([
                'MAC' => $mac_address,
                'type' => $type,
                'startDate' => $startDate,
                'endDate' => $endDate
            ]);

            if ($insertResult->getInsertedCount() == 0) {
                array_push($failed, array(
                    $mac_address => "insert failed"
                ));
            }
        }
    }

    $result = array(
        "message" => "complete",
        "errors" => $failed
    );
    return $result;
}

function editDowntimeEvent($id, $mac_address, $type, $startDate, $endDate) {
    global $client;
    $downtime_events = $client->fluent->downtime_events;

    $updateResult = $downtime_events->updateOne(['_id' => $id],
        ['$set' => [
            'MAC' => $mac_address,
            'type' => $type,
            'startDate' => $startDate,
            'endDate' => $endDate
        ]]
    );

    if ($updateResult->getMatchedCount() == 0) {
        $result = array(
            "error" => "no record with this id"
        );
        return $result;
    }

    $result = array(
        "message" => "record updated successfully"
    );
    return $result;
}

function deleteDowntimeEvent($id) {
    global $client;
    $downtime_events = $client->fluent->downtime_events;

    $deleteResult = $downtime_events->deleteOne(['_id' => $id]);

    if ($deleteResult->getDeletedCount() == 0) {
        $result = array(
            "error" => "no records deleted"
        );
        return $result;
    }

    $result = array(
        "message" => "record deleted successfully"
    );
    return $result;
}
