<?php
require_once '/var/www/html/backend/vendor/autoload.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

generate_daily_charts();

$date = date('m/d/Y h:i:s a', time());
echo $date."\n";

function generate_daily_charts(){
	global $client;
	
	//loading sensor_status table
    	$collection = $client->fluent->sensor_data_archive;
	
	$cpu_caching = $client->fluent->cpu_cache;
	$ram_caching = $client->fluent->ram_cache;
	$storage_caching = $client->fluent->storage_cache;
	$net_caching = $client->fluent->network_cache;
	
	$sensors_arr = $client->fluent->sensor_matrix->find([])->toArray();
	
	$one_day = 24 * 60 * 60 * 1000;
	
	$offset = 8 * 60 * 60 * 1000; //8 hrs in milliseconds. Represents the time difference between UTC and SGT.
	
	foreach($sensors_arr as $sensor) {
		$mac = $sensor['MAC'];
		$pause_status = $sensor['pause_status'];

		for ($i = 15; $i <= 30; $i += 15){
			$i_sec = $i * 60 * 1000;
			
			#$day_start = 1479830400 * 1000; #23/11/2016 midnight SGT
			
			$timenow = (time() * 1000);
			
			$day_start =  $timenow - ($timenow%$one_day) - $offset; #last midnight SGT
			
			#while(($timenow - $day_start) > $one_day) {
			$UTCDT_daystart = new MongoDB\BSON\UTCDateTime($day_start);
			
			$number_of_blocks = 24 * 60 / $i; //60%$interval should give whole number
				
			//store the mode for each x min block
			$cpu_arr = array();
			$ram_arr = array();
			$storage_arr = array();
			$network_arr = array();
			
			$time_start = $day_start;
			
			for ($j = 1; $j <= $number_of_blocks; $j++){
				$block_result = array();
				
				//Imagine: from 20/7/2016 12am to 27/7/2016 12am...
				$time_from = new MongoDB\BSON\UTCDateTime($time_start);

				$time_end = $time_start + $i_sec;

				$time_to = new MongoDB\BSON\UTCDateTime($time_end);

				$query = array();
				$query['time'] = array();
				$query['time']['$gt'] = $time_from;
				$query['time']['$lt'] = $time_to;
				$query['MAC'] = $mac;

				$data_arr = $collection->find($query)->toArray();

				//change from here
				$cpu_total = 0;
				$ram_total = 0; //for counting
				$total_ram = 0; //max ram in the sensor
				$storage_total = 0; //for counting
				$total_storage = 0; //max storage space in the sensor
				$network_up_total = 0;
				$network_down_total = 0;

				$data_count = count($data_arr);
				
				if ($data_count == 0){
					$cpu_avg = -1;
					$ram_avg = -1;
					$total_ram = -1;
					$storage_avg = -1;
					$total_storage = -1;
					$network_up_avg = -1;
					$network_down_avg = -1;
				} else {
					foreach ($data_arr as $data){
						$cpu = $data['CPU'];
						$ram = $data['RAM_Used'];
						$total_ram = $data['RAM_Total'];
						$storage = $data['Disk_Space_Used'];
						$total_storage = $data['Disk_Space_Total'];
						$network_up = $data['Network_Upload_Speed'];
						$network_down = $data['Network_Download_Speed'];

						$cpu_total += $cpu;
						$ram_total += $ram;
						$storage_total += $storage;
						$network_up_total = $network_up;
						$network_down_total = $network_down;
					}
					
					$cpu_avg = $cpu_total/$data_count;
					$ram_avg = $ram_total/$data_count;
					$storage_avg = $storage_total/$data_count;
					$network_up_avg = $network_up_total/$data_count;
					$network_down_avg = $network_down_total/$data_count;
				}
				

				//Date Time
				$datetime = $time_from->toDateTime()->setTimezone(new DateTimeZone('Singapore'));
				$datetime_string = $datetime->format('Y-m-d H:i:s');

				$block_result_cpu["timestamp"] = $datetime_string;
				$block_result_ram["timestamp"] = $datetime_string;
				$block_result_storage["timestamp"] = $datetime_string;
				$block_result_net["timestamp"] = $datetime_string;
				
				$block_result_cpu["average"] = $cpu_avg;
				$block_result_ram["average"] = $ram_avg;
				$block_result_storage["average"] = $storage_avg;
				$block_result_net["up_average"] = $network_up_avg;
				$block_result_net["down_average"] = $network_down_avg;
				
				$block_result_ram["total"] = $total_ram;
				$block_result_storage["total"] = $total_storage;

				array_push($cpu_arr, $block_result_cpu);
				array_push($ram_arr, $block_result_ram);
				array_push($storage_arr, $block_result_storage);
				array_push($network_arr, $block_result_net);
				
				$time_start = $time_end;
			}
			
			if ($pause_status == true){
				$cpu_arr = null;
				$ram_arr = null;
				$storage_arr = null;
				$network_arr = null;
			}
			
			$tobeInserted_cpu = array();
			$tobeInserted_cpu['mac'] = $mac;
			$tobeInserted_cpu['date'] = $UTCDT_daystart;
			$tobeInserted_cpu['interval'] = strval($i);
			$tobeInserted_cpu['data'] = $cpu_arr;
			
			$cpu_caching->insertOne($tobeInserted_cpu);
			
			$tobeInserted_ram = array();
			$tobeInserted_ram['mac'] = $mac;
			$tobeInserted_ram['date'] = $UTCDT_daystart;
			$tobeInserted_ram['interval'] = strval($i);
			$tobeInserted_ram['data'] = $ram_arr;
			
			$ram_caching->insertOne($tobeInserted_ram);
			
			$tobeInserted_storage = array();
			$tobeInserted_storage['mac'] = $mac;
			$tobeInserted_storage['date'] = $UTCDT_daystart;
			$tobeInserted_storage['interval'] = strval($i);
			$tobeInserted_storage['data'] = $storage_arr;
			
			$storage_caching->insertOne($tobeInserted_storage);

			$tobeInserted_net = array();
			$tobeInserted_net['mac'] = $mac;
			$tobeInserted_net['date'] = $UTCDT_daystart;
			$tobeInserted_net['interval'] = strval($i);
			$tobeInserted_net['data'] = $network_arr;
			
			$net_caching->insertOne($tobeInserted_net);
				
				#echo "data inserted\n";
				
				#$day_start += $one_day;
			#}
		}
		#echo "end\n";
	}
}
?>
