<?php

require 'vendor/autoload.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

function updateUniversalSettings($dnt, $ed, $ft){
	$settings = $client->fluent->universal_settings
	
	#$cmd = "sudo sed -i '/'snmp-speedtest.sh'/c\\".$dnt." root bash /home/pi/snmp-speedtest.sh' /etc/cron.d/snmp-speedtest";
	
	$toBeInserted = array();
	$toBeInserted['daily_notification_time'] = $dnt;
	$toBeInserted['elapsed_downtime'] = $ed;
	$toBeInserted['flapping_threshold'] = $ft;

	$updateResult = $sensors->replaceOne([], $toBeInserted, ['upsert'=>true]);

	if ($updateResult->getMatchedCount() == 0 && $updateResult->getUpsertedCount() == 0){
		$error = array(
			"status" => 502,
			"error" => "Record not updated due to an error."
		);
		return json_encode($error);
	} else {
		$error = array(
			"status" => 200,
			"success" => "Record successfully updated."
		);
		return json_encode($error);
	}
}

?>