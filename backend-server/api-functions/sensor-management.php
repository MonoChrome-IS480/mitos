<?php

require '../vendor/autoload.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");
//$sensors = $client->fluent->sensor_matrix;

function InsertNewSensor($mac_address, $geo_region, $building, $sensor_location_level, $sensor_location_id, $watchlist=false, $port="", $thresholds){
	if ($watchlist == "") {
		$watchlist = false;
	}

	global $client;
	$sensors = $client->fluent->sensor_matrix;

	//check if sensor already exists
	$queryMac = array('MAC' => $mac_address);

	$queryID = array();
	$queryID['building'] = $building;
	$queryID['sensor-location-level'] = $sensor_location_level;
	$queryID['sensor-location-id'] = $sensor_location_id;

	$findSensorMAC = $sensors->find($queryMac);
	$findSensorID = $sensors->find($queryID);

	$macCheck = count($findSensorMAC->toArray());
	$idCheck = count($findSensorID->toArray());

	if ($macCheck > 0) {
		$error = array(
			"status" => 502,
			"error" => "sensor with this mac address already exists"
		);
		return json_encode($error);
	} elseif ($idCheck > 0) {
		$error = array(
			"status" => 502,
			"error" => "sensor with this building, level and id already exists"
		);
		return json_encode($error);
	}

	$toBeInserted = array();
	$toBeInserted['MAC'] = $mac_address;
	$toBeInserted['geo-region'] = $geo_region;
	$toBeInserted['building'] = $building;
	$toBeInserted['sensor-location-level'] = $sensor_location_level;
	$toBeInserted['sensor-location-id'] = $sensor_location_id;
	$toBeInserted['watchlist'] = $watchlist;
	$toBeInserted['port'] = $port;
	$toBeInserted['thresholds'] = $thresholds;
	$toBeInserted['pause_status'] = false;

	$writeResult = $sensors->insertOne($toBeInserted);

	if ($writeResult->getInsertedCount() == 0){
		$error = array(
			"status" => 502,
			"error" => "Error inserting record"
		);
		return json_encode($error);
	} else {
		$error = array(
			"status" => 200,
			"success" => "Record inserted"
		);
		return json_encode($error);
	}
}

function UpdateNewSensor($mac_address, $geo_region, $building, $sensor_location_level, $sensor_location_id, $watchlist=false, $port="", $thresholds){
	if ($watchlist == "") {
		$watchlist = false;
	}

	global $client;
	$sensors = $client->fluent->sensor_matrix;

	// $count = count($sensors->find(['MAC'=>$mac_address])->toArray());
    //
	// if ($count == 0) {
	// 	$error = array(
	// 		"status" => 200,
	// 		"error" => "Mac Address does not exist!"
	// 	);
	// 	return json_encode($error);
	// }
    $sensor = $sensors->findOne(['MAC'=>$mac_address]);

    if ($sensor == null) {
        $error = array(
			"status" => 200,
			"error" => "Mac Address does not exist!"
		);
		return json_encode($error);
    }

    $queryID = array(
        'building' => $building,
        'sensor-location-level' => $sensor_location_level,
        'sensor-location-id' => $sensor_location_id
    );

	$idCheck = $sensors->findOne($queryID);

    if ($idCheck!=null && $idCheck['MAC']!=$mac_address) {
        $error = array(
			"status" => 502,
			"error" => "another sensor with this building, level and id already exists"
		);
		return json_encode($error);
    }


	$toBeInserted = array();
	$toBeInserted['MAC'] = $mac_address;
	$toBeInserted['geo-region'] = $geo_region;
	$toBeInserted['building'] = $building;
	$toBeInserted['sensor-location-level'] = $sensor_location_level;
	$toBeInserted['sensor-location-id'] = $sensor_location_id;
	$toBeInserted['watchlist'] = $sensor['watchlist'];
	$toBeInserted['port'] = $port;
	$toBeInserted['thresholds'] = $thresholds;
	$toBeInserted['pause_status'] = $sensor['pause_status'];

	$updateResult = $sensors->replaceOne(['MAC'=>$mac_address], $toBeInserted, ['upsert'=>true]);

	if ($updateResult->getMatchedCount() == 0 && $updateResult->getUpsertedCount() == 0){
		$error = array(
			"status" => 502,
			"error" => "Record not updated due to an error."
		);
		return json_encode($error);
	} else {
		$error = array(
			"status" => 200,
			"success" => "Record successfully updated."
		);
		return json_encode($error);
	}
}

function deleteExistingSensor($mac_address){
	global $client;
	$sensors = $client->fluent->sensor_matrix;

	$deleteResult = $sensors->deleteOne(['MAC'=>$mac_address]);

	if($deleteResult->getDeletedCount() == 0){
		$error = array(
			"status" => 502,
			"error" => "Record not deleted due to an error."
		);
		return json_encode($error);
	} else {
		$success = array(
			"status" => 200,
			"success" => "Record successfully deleted."
		);
		return json_encode($success);
	}
}

function viewAllSensors(){
	global $client;
	$sensors = $client->fluent->sensor_matrix;

	$result = array();

	$all_sensors = $sensors->find([])->toArray();

	foreach ($all_sensors as $sensor) {
		$mac_address = $sensor['MAC'];
		$geo_region = $sensor['geo-region'];
		$building = $sensor['building'];
		$sensor_location_level = $sensor['sensor-location-level'];
		$sensor_location_id = $sensor['sensor-location-id'];

		$info = array();
		$info['geo_region'] = $geo_region;
		$info['building'] = $building;
		$info['sensor_location_level'] = $sensor_location_level;
		$info['sensor_location_id'] = $sensor_location_id;

		$result[$mac_address] = $info;
	}

	return $result;
}

//watchlist pinning
function watchlistPin($mac_address, $pinstatus){
	global $client;
	$sensors = $client->fluent->sensor_matrix;

	$count = count($sensors->find(['MAC'=>$mac_address])->toArray());

	if ($count == 0) {
		$error = array(
			"status" => 200,
			"error" => "Mac Address does not exist!"
		);
		return json_encode($error);
	}

	$updateResult = $sensors->updateOne(['MAC'=>$mac_address],['$set'=>['watchlist'=>$pinstatus]], ["upsert"=>true]);

	if($updateResult->getMatchedCount() > 0){
		$success = array(
			"status" => 200,
			// "error" => "Record successfully pinned."
		);
		if ($pinstatus) {
			$success["success"] = "Record successfully pinned.";
		} else {
			$success["success"] = "Record successfully unpinned.";
		}
		return json_encode($success);
	} else {
		$error = array(
			"status" => 502,
			"error" => "Record not updated due to an error."
		);
		return json_encode($error);
	}
}

//check for sensor paused
function pause_sensors($mac_addresses, $paused){
	global $client;
	$sensors = $client->fluent->sensor_matrix;

    $sensor_data = $client->fluent->sensor_data;
    $sensor_data_archive = $client->fluent->sensor_data_archive;

    $bigResult = array();

    foreach ($mac_addresses as $mac_address) {
        $count = count($sensors->find(['MAC'=>$mac_address])->toArray());

        if (!preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $mac_address)) {
        	$error = array(
                "macAdd" => $mac_address,
        		"error" => "invalid mac address"
        	);
    		array_push($bigResult, $error);
        } else {
            if ($count == 0) {
        		$error = array(
                    "macAdd" => $mac_address,
        			"error" => "Mac Address does not exist!"
        		);
        		array_push($bigResult, $error);
        	} else {
                $data = $sensor_data->findOne(['MAC'=>$mac_address]);
                $archive = $sensor_data_archive->findOne(['MAC'=>$mac_address]);

                if ($data = null || $archive == null) {
                    $error = array(
                        "macAdd" => $mac_address,
                        "error" => "Sensor has no data."
                    );
                    array_push($bigResult, $error);
                } else {
                    $updateResult = $sensors->updateOne(['MAC'=>$mac_address],['$set'=>['pause_status'=>$paused]], ["upsert"=>false]);

                	if($updateResult->getMatchedCount() > 0){
                		$success = array("macAdd" => $mac_address);
                		if ($paused) {
                			$success["success"] = "Record successfully paused.";
                		} else {
                			$success["success"] = "Record successfully unpaused.";
                		}
                        array_push($bigResult, $success);
                	} else {
                		$error = array(
                            "macAdd" => $mac_address,
                			"error" => "Record not updated due to an error."
                		);
                		array_push($bigResult, $error);
                	}
                }
            }

        }

    }

    return $bigResult;

}

// retrievePortIdentificationForInitializationOfRebootSequence
function RPIFIORS($mac_address) {
	global $client;
	$sensors = $client->fluent->sensor_matrix;

	$senArr = $sensors->find(['MAC'=>$mac_address])->toArray();

	$port = $senArr[0]["port"];

	return $port;

}

function getSensorsInBuilding($building) {
    global $client;
	$sensors = $client->fluent->sensor_matrix;

	$senArr = $sensors->find(
        ['building'=>$building],
        [
            'projection' => [
                'MAC' => 1,
                'sensor-location-level' => 1,
                'sensor-location-id' => 1,
                'pause_status' => 1
            ]
        ]
    )->toArray();

    if (count($senArr) > 0) {
        $superResult = array(
            "building" => $building
        );

        $result = array();
        foreach ($senArr as $sensor) {
            $subResult = array(
                "macAdd" => $sensor['MAC'],
                "areaId" => $sensor['sensor-location-id'],
                "location" => $sensor['sensor-location-level'],
                "paused_status" => $sensor['pause_status']
            );
            if ($sensor['pause_status'] == null) {
                $subResult['paused_status'] = false;
            }
            array_push($result, $subResult);
        }

        $superResult["all_sensors"] = $result;

        return $superResult;
    } else {
        $result = array(
            "error" => "no building found"
        );
        return $result;
    }


}

function getAllBuildings() {
    global $client;
	$sensors = $client->fluent->sensor_matrix;

	$result = array();

	$all_sensors = $sensors->find([],[
        'projection' => ['building' => 1]
    ])->toArray();

	foreach ($all_sensors as $sensor) {
        $subResult = array(
            "label" => $sensor['building'],
            "value" => $sensor['building']
        );
        if (!in_array($subResult, $result)) {
            array_push($result, $subResult);
        }
	}

	return $result;
}

function getSensorInfo($mac_address) {
    global $client;
	$sensors = $client->fluent->sensor_matrix;

	$sensor = $sensors->findOne(['MAC'=>$mac_address]);

	if ($sensor == null) {
		$error = array(
			"status" => 200,
			"error" => "Mac Address does not exist!"
		);
		return json_encode($error);
	}

    unset($sensor['_id']);
    return $sensor;

}
