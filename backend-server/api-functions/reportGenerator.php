<?php
// Pear Mail Library
include_once("Mail.php");
include_once("Mail/mime.php");

require_once '/var/www/html/backend/vendor/autoload.php';
require_once '/var/www/html/backend/websocket-functions/common/common-functions.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

generateDailyReport();

function generateDailyReport(){
	global $client;
	
	$uni_settings = $client->fluent->universal_settings;
	$settings = $uni_settings->findOne([]);
	
	// Constructing the email
	$sender = "is480mc2016@gmail.com";                       // Your name and email address
	$recipient = $settings['report_recipient'];          // The Recipients name and email address
	$text = 'This is a text message.';						 //for backup incase HTML message not working
	
	//Establish connections to collections
	$sensor_matrix = $client->fluent->sensor_matrix;
	$server_matrix = $client->fluent->server_matrix; #not in use yet
	
	$sensor_data = $client->fluent->sensor_data;
	$sensor_data_arc = $client->fluent->sensor_data_archive;
	
	//Generate Time and Email Header
	$timenow = time();
	$one_day = 24 * 60 * 60; //one day in seconds
	$offset = 8 * 60 * 60;
	
	$time_ytd = $timenow - ($timenow%$one_day) - $offset;

	$timezone = new DateTimeZone('Singapore');
	
	$datetime = new DateTime(null, $timezone);
	
	$datetime_format = 'Y/m/d';
	
	$date_format = $datetime->format($datetime_format);
	
	$subject = "MITOS Daily Summary Report for ".$date_format;   //Email Subject
	
	//Get all Sensors
	$sensor_arr = $sensor_matrix->find([]);
	
	//Current sensor information generator
	$sensors_down = 0;
	$sensors_danger = 0;
	
	$down_sensors_report = array();
	$danger_sensors_report = array();
	$site_w_issues_report = array();
	
	foreach ($sensor_arr as $sensor){
		$mac = $sensor['MAC'];
		$building = $sensor['building'];
		$level = $sensor['sensor-location-level'];
		$id = $sensor['sensor-location-id'];
		
		$status = checkStatus($mac);
		
		if ($status == "down"){
			//echo "here\n";
			
			$sensors_down++;
			
			#pull last record from DB
			$last_data_set = $sensor_data->findOne(['MAC'=>$mac], ['sort'=> ['time' => -1]]);
			
			if (count($last_data_set) == 0){ #no data in $sensor_data collection, means down for > 24 hours
				$last_data_set = $sensor_data_arc->findOne(['MAC'=>$mac], ['sort'=> ['time' => -1]]);
			}
			
			$timestamp = $last_data_set['time']; #bson timestamp
			
			$ts_dt = $timestamp->toDateTime();
			
			$ts_dt_format = $ts_dt->format('Y-m-d H:i:s');
			
			array_push($down_sensors_report,'<tr><td>'.$building." level ".$level.$id.'</td><td>'.$mac.'</td><td>'.$ts_dt_format.'</td></tr>');
			
			#pull last record from DB
			$last_data_set = $sensor_data->findOne(['MAC'=>$mac], ['sort'=> ['time' => -1]]);
			
			if (count($last_data_set) == 0){ #no data in $sensor_data collection, means down for > 24 hours
				$last_data_set = $sensor_data_arc->findOne(['MAC'=>$mac], ['sort'=> ['time' => -1]]);
			}
			
			$timestamp = $last_data_set['time']->toDateTime()->getTimestamp(); #bson timestamp
			
			$timenow = time();
			
			$difference = $timenow - $timestamp;
			
			$one_day = 3600 * 24; #amount of seconds in 1 day
			
			if ($difference > 1800) { #30 mins
				if (isset($site_w_issues_report[$building])) {
					$site_w_issues_report[$building]['count'] += 1;
				} else {
					$site_w_issues_report[$building] = array();
					$site_w_issues_report[$building]['building'] = $building;
					$site_w_issues_report[$building]['count'] = 1;
					
					if ($difference > $one_day) {
						
						$diff_days = floor($difference/$one_day);
						
						$site_w_issues_report[$building]['downtime'] = $diff_days." days";
					} elseif ($difference > 3600){
						$diff_hours = floor($difference/3600); 
						
						$site_w_issues_report[$building]['downtime'] = $diff_hours." hours";
					}
				}
			}
			
		} elseif ($status == "danger"){
			$sensors_danger++;
			
			array_push($danger_sensors_report,"<tr><td>".$building." level ".$level.$id.'</td><td>'.$mac.'</td></tr>');
		}
	}
	
	sort($down_sensors_report);
	sort($danger_sensors_report);
	
	echo "Completed\n";
	
	$header0 = '<h2>Overview for today</h2>'; #For sites with issues.
	
	#$site_w_issues_final = array();
	
	if (count($site_w_issues_report) == 0){
		$header0 .= '<ul><li>There are no sites with issues</li></ul>';
	} else {
		$header0 .= '<table><tr><th>Site</th><th>No. of Sensors Down</th><th>Remarks</th></tr>';
		
		foreach($site_w_issues_report as $site){
			$building = $site['building'];
			$count = $site['count'];
			$downtime = $site['downtime'];
			
			
			$header0 .= "<tr><td>".$building."</td><td>".$count." sensor(s) down</td><td>Highest Sensor Downtime is ~".$downtime.'</td></tr>';
		}
		
		$header0 .= '</table>';
	}
	
	$header1 = '<h3><b>Current Down sensors:</b> '.$sensors_down.'</h3>';
	
	if (count($down_sensors_report) > 0){
		$header1 .= '<table><tr><th>Location</th><th>Mac Address</th><th>Last Uptime</th></tr>';
		foreach($down_sensors_report as $down_report){
			$header1 .= $down_report;
		}
		$header1 .= '</table>';
	} else {
		$header1 .= '<ul><li>There are no sensors down currently.</li></ul>';
	}
	
	$header2 = ' <h3><b>Current Danger sensors:</b> '.$sensors_danger.'</h3>';
	
	if (count($danger_sensors_report) > 0){
		$header2 .= '<table><tr><th>Location</th><th>Mac Address</th></tr>';
		foreach($danger_sensors_report as $danger_report){
			$header2 .= $danger_report;
		}
		$header2 .= '</table>';
	} else {
		$header2 .= '<ul><li>There are no sensors in danger currently.</li></ul>';
	}
	
	//end of current sensor information generator
	
	//start of past 24 hours information generator
	$notifications_collection = $client->fluent->notifications;
	
	$yesterday_bson = new MongoDB\BSON\UTCDateTime($time_ytd * 1000); //construct in milliseconds
	
	$noti_downs = array();
	$noti_flappings = array();
	$noti_abnormals = array();
	
	$sensor_arr1 = $sensor_matrix->find([],['sort'=>['building'=>1,'sensor-location-level'=>1,'sensor-location-id'=>1]]);
	foreach ($sensor_arr1 as $sensor){
		$mac = $sensor['MAC'];
		$building = $sensor['building'];
		$level = $sensor['sensor-location-level'];
		$id = $sensor['sensor-location-id'];
		
		$query = array();
		$query['timestamp'] = array();
		$query['timestamp']['$gte'] = $yesterday_bson;
		$query['mac'] = $mac;
		
		$notifications_arr = $notifications_collection->find($query);
		
		$noti_down = 0;
		$noti_flapping = 0;
		$noti_abnormal = 0;
		
		foreach ($notifications_arr as $notifications){
			$problem = $notifications['problem'];
			$noti_status = $problem['status'];
			
			if($noti_status == "down"){
				$noti_down++;
			} elseif ($noti_status == "Flapping"){
				$noti_flapping++;
			} elseif ($noti_status == "Abnormal") {
				$noti_abnormal++;
			}
		}
		
		if ($noti_down > 0){
			array_push($noti_downs,'<tr><td>'.$building.' level '.$level.$id.'</td><td>'.$noti_down.'</td></tr>');
		}
		if ($noti_flapping > 0){
			array_push($noti_flappings,'<tr><td>'.$building.' level '.$level.$id.'</td><td>'.$noti_flapping.'</td></tr>');
		}
		if ($noti_abnormal > 0){
			array_push($noti_abnormals,'<tr><td>'.$building.' level '.$level.$id.'</td><td>'.$noti_abnormal.'</td></tr>');
		}
	}
	//end of past 24 hours information generator
	
	$header3 = '<hr><h2>For past 24 hours</h2> ';
	$header3 .= '<h3><b>Sensors Down in the Past 24 Hours: </b>'.count($noti_downs).'</h3>';
	
	if (count($noti_downs) > 0){
		$header3 .= '<table align="center" style=\'width:100%\'><tr><th>Physical Location</th><th>Frequency</th></tr>';
		foreach($noti_downs as $down_alert){
			$header3 .= $down_alert;
		}
		$header3 .= '</table>';
	} else {
		$header3 .= '<ul><li>There are no Down sensors for the past 24 hours</li></ul>';
	}
	
	$header4 = ' <h3><b>Sensors Flapping in the Past 24 Hours: </b>'.count($noti_flappings).'</h3>';
	
	if (count($noti_flappings) > 0){
		$header4 .= '<table align="center" style=\'width:100%\'><tr><th>Physical Location</th><th>Frequency</th></tr>';
		foreach($noti_flappings as $flapping_alert){
			$header4 .= $flapping_alert;
		}
		$header4 .= '</table>';
	} else {
		$header4 .= '<ul><li>There are no Flapping sensors for the past 24 hours</li></ul>';
	}
	
	$header5 = '<h3><b>Sensors Abnormal in the Past 24 Hours: </b>'.count($noti_abnormals).'</h3>';
	
	if (count($noti_abnormals) > 0){
		$header4 .= '<table align="center" style=\'width:100%\'><tr><th>Physical Location</th><th>Frequency</th></tr>';
		foreach($noti_abnormals as $abnormals_alert){
			$header5 .= $abnormals_alert;
		}
		$header5 .= '</table>';
	} else {
		$header5 .= '<ul><li>There are no Abnormal sensors for the past 24 hours</li></ul>';
	}
	
	//final piece-ing of content message
	$content = $header0.$header1.$header2.$header3.$header4.$header5; 
	
	$cerebus_msg = 
	'<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn\'t be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
    
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: Century Gothic !important;
            }
        </style>
    <![endif]-->
    
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
        <!-- insert web font reference, eg: <link href=\'https://fonts.googleapis.com/css?family=sans-serif:400,700\' rel=\'stylesheet\' type=\'text/css\'> -->
		<link href=\'http://fonts.googleapis.com/css?family=Roboto:300,400\' rel=\'stylesheet\' type=\'text/css\'>
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->
    
      <!-- CSS Reset -->
	 
    <style>
	
        # What it does: Remove spaces around the email design added by some email clients.
        # Beware: It can remove the padding / margin and add a background color to the compose a reply window.
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
			font-family: Century Gothic;
        }
        
        # What it does: Stops email clients resizing small text. 
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        # What is does: Centers email on Android 4.4 
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        # What it does: Stops Outlook from adding extra spacing to tables. 
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        # What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. 
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        # What it does: Uses a better rendering method when resizing images in IE. 
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        # What it does: A work-around for iOS meddling in triggered links. 
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        # What it does: Hover styles for buttons 
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }
		
		table tr td{
            padding: 0.5rem;
			text-align: center;
			font-family: Century Gothic;
			font-size: 15px;
			mso-height-rule: exactly;
			line-height: 20px;
			color: #555555;
			width: 33.3%
        }
		
		table {
			align: center;
			width: 100%;
			border: 1;
		}
		
		h1, h2, h3 {
            text-align: left
        }

		
    </style>

</head>
<body width="100%" bgcolor="#f2f2f2" style="margin: 0;">
    <center style="width: 100%; background: #f2f2f2;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: CenturyGothic;">
            MITOS Daily Summary Report for the day
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!--    
            Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
            2. MSO tags for Desktop Windows Outlook enforce a 600px width.
        -->
        <div style="max-width: 600px; margin: auto;">
            <!--[if mso]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
            <td>
            <![endif]-->

            <!-- Email Header : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                <tbody><tr>
                    <td style="padding: 20px 0; text-align: center">
                        <img src="http://119.81.104.44/wp-content/uploads/2016/08/sence_logo_sticker_black_25jul2016.png" width="234" height="108" alt="SENCE LOGO" border="0" style="height: auto; background: #dddddd; font-family: CenturyGothic; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                    </td>
                </tr>
            </tbody></table>
            <!-- Email Header : END -->
            
            <!-- Email Body : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody><tr>
                                <td style="padding: 40px; font-family: Century Gothic; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    <h1 style="text-align:center">Daily MITOS Summary Report</h1>
									<h2 style="text-align:center">'.$date_format.'</h2>
									<hr size="5" color="#000000">
									'.$content.'
									 
									 
                                    <!-- Button : Begin -->
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
                                        <tbody><tr>
                                            <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                <a href="http://opsdev.sence.io/" style="background: #222222;border: 15px solid #222222;font-family: Century Gothic;font-size: 13px;line-height: 1.1;/* text-align: center; */text-decoration: none;/* display: block; */border-radius: 3px;float: left;font-weight: bold;" class="button-a">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffffff">Bring me to the Dashboard!</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    <!-- Button : END -->
                                    </td>
                                </tr>
                        </tbody></table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : BEGIN -->

                <!-- Clear Spacer : BEGIN -->
                <tr>
                    <td height="40" style="font-size: 0; line-height: 0;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Clear Spacer : END -->

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody><tr>
                                <td style="padding: 20; font-family: Century Gothic; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    <center><b>*This is a System Generated Email, please do not reply.</b></center>
                                </td>
                                </tr>
                        </tbody></table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : BEGIN -->

            </tbody></table>
            <!-- Email Body : END -->
          
            <!-- Email Footer : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                <tbody><tr>
                    <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: Century Gothic; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                         
                        <span class="mobile-link--footer">Team MonoChrome@2016</span> 
                         
                    </td>
                </tr>
            </tbody></table>
            <!-- Email Footer : END -->

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>

</body></html>';
	
	$crlf = "\n";
	$headers = array(
					'From'          => $sender,
					'Return-Path'   => $sender,
					'Subject'       => $subject
					);

	// Creating the Mime message
	$mime = new Mail_mime($crlf);

	// Setting the body of the email
	$mime->setTXTBody($text);
	$mime->setHTMLBody($cerebus_msg);

	$body = $mime->get();
	$headers = $mime->headers($headers);

	$smtp = Mail::factory('smtp', array(
			'host' => 'ssl://smtp.gmail.com',
			'port' => '465',
			'auth' => true,
			'username' => 'is480mc2016@gmail.com',
			'password' => 'asdf0000'
		));

	$mail = $smtp->send($recipient, $headers, $body);
	
	if (PEAR::isError($mail)) {
		echo('<p>' . $mail->getMessage() . '</p>');
	} else {
		echo('<p>Message successfully sent!</p>');
	}
}
?>