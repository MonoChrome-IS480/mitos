<?php
require_once '/var/www/html/backend/vendor/autoload.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

function generate_chart_data($mac, $start_date, $end_date, $interval, $metrics){
	global $client;

	$threshold_arr = array();
	
	$sensor_matrix = $client->fluent->sensor_matrix;
	$sensor_data = $client->fluent->sensor_data;
	$sensor_data_arc = $client->fluent->sensor_data_archive;

	$sensor_info = $sensor_matrix->findOne(['MAC'=>$mac]);
	
	if ($metrics != "uptime" || $metrics != "uptime_alt" || $metrics != "network"){
	#pull last record from DB
		$last_data_set = $sensor_data->findOne(['MAC'=>$mac], ['sort'=> ['time' => -1]]);

		if (count($last_data_set) == 0){ #no data in $sensor_data collection, means down for > 24 hours
			$last_data_set = $sensor_data_arc->findOne(['MAC'=>$mac], ['sort'=> ['time' => -1]]);
		}
	}

    //loading the correct cache table
	switch ($metrics){
		case ("uptime_alt"):
			$collection = $client->fluent->uptime_alternate;
			break;
		case ("uptime"):
			$collection = $client->fluent->uptime_cache;
			break;
		case ("cpu"):
			$collection = $client->fluent->cpu_cache;
			
			//in percentage
			$threshold_arr['max'] = 100;
			$threshold_arr['danger'] = $sensor_info['thresholds']['danger']['cpu_usage'];
			$threshold_arr['warning'] = $sensor_info['thresholds']['warning']['cpu_usage'];
			break;
		case ("ram"):
			$collection = $client->fluent->ram_cache;
			
			$total_ram = $last_data_set['RAM_Total'];
			$threshold_arr['max'] = $total_ram;
			$threshold_arr['danger'] = $sensor_info['thresholds']['danger']['ram_usage'] * $total_ram;
			$threshold_arr['warning'] = $sensor_info['thresholds']['warning']['ram_usage'] * $total_ram;
			break;
		case ("storage"):
			$collection = $client->fluent->storage_cache;
			
			$total_disk = $last_data_set['Disk_Space_Total'];
			$threshold_arr['max'] = $total_disk;
			$threshold_arr['danger'] = $sensor_info['thresholds']['danger']['disk_usage'] * $total_disk;
			$threshold_arr['warning'] = $sensor_info['thresholds']['warning']['disk_usage'] * $total_disk;
			break;
		case ("network"):
			$collection = $client->fluent->network_cache;
			break;
		//NO DEFAULT. Must be 1 of the 6.
	}

	$one_day = 24 * 60 * 60; //one day in seconds

	$timezone = new DateTimeZone('Singapore');

	$start_date_DT = new DateTime($start_date, $timezone);
	$end_date_DT = new DateTime($end_date, $timezone);

	$start_date_TS = $start_date_DT->getTimestamp(); //in seconds
	$end_date_TS = $end_date_DT->getTimestamp(); //in seconds

	$period = ((($end_date_TS-$start_date_TS)/$one_day) + 1)*24;

	$start_date_bson = new MongoDB\BSON\UTCDateTime(($start_date_TS) * 1000); //construct in milliseconds
	$end_date_bson = new MongoDB\BSON\UTCDateTime(($end_date_TS) * 1000); //construct in milliseconds

	$query = array();
	$query['date'] = array();
	$query['date']['$gte'] = $start_date_bson;
	$query['date']['$lte'] = $end_date_bson;
	$query['mac'] = $mac;
	$query['interval'] = $interval;

	$data_arr = $collection->find($query, ['sort'=>['date'=>1]])->toArray();

	$data_arr_count = count($data_arr);

	$lack_data_count = 0;
	//echo $data_arr_count.'\n';

	//store the result to return
	$result_arr = array();

	for ($i = 0; $i < $data_arr_count; $i++){
		$data = $data_arr[$i];

		$uptime_info = $data['data'];

		$lack_data = 0;
		$block_counts = count($uptime_info);

		for ($j = 0; $j < $block_counts; $j++){
			//BSONDocument
			$block_info = $uptime_info[$j];

			$info = array();

			$block_info_count = 0;

			foreach ($block_info as $block_info_arr){

				if ($block_info_count == 0) {
					$info["timestamp"] = $block_info_arr;
				} elseif ($block_info_count == 1) {
					if ($block_info_arr != -1){
						if ($metrics == "network"){
							$info["network_up"] = $block_info_arr;
						} elseif ($metrics == "uptime" || $metrics == "uptime_alt"){
							$info["status"] = $block_info_arr;
							if ($metrics == "uptime"){
								$info["value"] = 1;
							}
						} else {
							$info["value"] = $block_info_arr;
						}
					} else {
						$lack_data++;
					}
				} else { //means got 3 values in $block info
					if ($block_info_arr != -1){
						if ($metrics == "network"){
							$info["network_down"] = $block_info_arr;
						} elseif ($metrics == "ram" || $metrics == "storage") {
							$info["max"] = $block_info_arr;
						} elseif ($metrics == "uptime_alt"){
							$info["duration"] = $block_info_arr; //abs. hours
						}
					}
				}

				$block_info_count++;
			}

			array_push($result_arr, $info);
		}

		if ($lack_data == $block_counts){
			$lack_data_count++;
		}
	}

	if ($metrics == "uptime" || $metrics == "uptime_alt" || $metrics == "network"){
		return $result_arr;
	}
	
	$final_result = array();
	
	if ($lack_data_count == $data_arr_count){ //no data for entire period
		array_push($final_result, "No Data Available");
		//push threshold values inside
	} else {
		array_push($final_result, "Data Available");
	}

	array_push($final_result, $threshold_arr);
	array_push($final_result, $result_arr);

	return $final_result;
}


function generateByBuilding($building_name, $start_date, $end_date, $interval="30", $metrics="uptime"){
	global $client;

	$building_result = array();

    //loading sensor_matrix table where the sensors belong to the specified building
    $sensors_arr = $client->fluent->sensor_matrix->find(['building'=>$building_name])->toArray();

	foreach ($sensors_arr as $sensor){
		$mac_add = $sensor['MAC'];
		$sensor_level = $sensor['sensor-location-level'];
		$sensor_id = $sensor['sensor-location-id'];

		//create array for that level if it doesn't exist
        if (!array_key_exists($sensor_level, $building_result)) {
            $building_result[$sensor_level] = array();
        }

		//create array for that level if it doesn't exist
        if (!array_key_exists($sensor_id, $building_result[$sensor_level])) {
            $building_result[$sensor_level][$sensor_id] = array();
        }

		$building_result[$sensor_level][$sensor_id]['mac'] = $mac_add;
		$building_result[$sensor_level][$sensor_id]['building'] = $building_name;
		$building_result[$sensor_level][$sensor_id]['level'] = $sensor_level;
		$building_result[$sensor_level][$sensor_id]['id'] = $sensor_id;
		$building_result[$sensor_level][$sensor_id]['data'] = generate_chart_data($mac_add, $start_date, $end_date, $interval, $metrics);
	}

	return $building_result;
}

function generateByBuildingAlt($building_name, $start_date, $end_date, $interval=30, $metrics="uptime_alt"){
	global $client;

	$building_result = array();

    //loading sensor_matrix table where the sensors belong to the specified building
    $sensors_arr = $client->fluent->sensor_matrix->find(['building'=>$building_name])->toArray();

	foreach ($sensors_arr as $sensor){
		$mac_add = $sensor['MAC'];
		$sensor_level = $sensor['sensor-location-level'];
		$sensor_id = $sensor['sensor-location-id'];

		//create array for that level if it doesn't exist
        if (!array_key_exists($sensor_level, $building_result)) {
            $building_result[$sensor_level] = array();
        }

		//create array for that level if it doesn't exist
        if (!array_key_exists($sensor_id, $building_result[$sensor_level])) {
            $building_result[$sensor_level][$sensor_id] = array();
        }

		$building_result[$sensor_level][$sensor_id]['mac'] = $mac_add;
		$building_result[$sensor_level][$sensor_id]['building'] = $building_name;
		$building_result[$sensor_level][$sensor_id]['level'] = $sensor_level;
		$building_result[$sensor_level][$sensor_id]['id'] = $sensor_id;
		$building_result[$sensor_level][$sensor_id]['data'] = generate_chart_data($mac_add, $start_date, $end_date, $interval, $metrics);
	}

	return $building_result;
}

//must disable these 3 for the API to work, below 3 codes STRICTLY for TESTING only
//echo json_encode(generateByBuilding('KRH','2016-11-20','2016-11-22','30'));

?>
