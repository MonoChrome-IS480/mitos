<?php
require '/var/www/html/backend/vendor/autoload.php';

//For uptime
$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

generate_daily_timechart();

$date = date('m/d/Y h:i:s a', time());
echo $date."\n";

function generate_daily_timechart(){
	global $client;
	
	//loading sensor_status table
	$collection = $client->fluent->sensor_status;
	
	$uptime_caching = $client->fluent->uptime_cache;
	
	$sensors_arr = $client->fluent->sensor_matrix->find([])->toArray();
	
	$one_day = 24 * 60 * 60 * 1000;
	
	$offset = 8 * 60 * 60 * 1000; //8 hrs in milliseconds. Represents the time difference between UTC and SGT.
	
	foreach($sensors_arr as $sensor) {
		$mac = $sensor['MAC'];

		for ($i = 15; $i <= 30; $i += 15){
			$i_sec = $i * 60 * 1000;
			
			#$day_start = 1474387200 * 1000; #21/09/2016 midnight SGT
			
			$timenow = (time() * 1000);
			
			$day_start =  $timenow - ($timenow%$one_day) - $offset; #last midnight SGT
			
			#while(($timenow - $day_start) > $one_day) {
			$UTCDT_daystart = new MongoDB\BSON\UTCDateTime($day_start);
			
			$number_of_blocks = 24 * 60 / $i; //60%$interval should give whole number
				
			//store the mode for each x min block
			$mode_arr = array();
			
			$time_start = $day_start;
			
			for ($j = 1; $j <= $number_of_blocks; $j++){
				$block_result = array();
				
				//Imagine: from 20/7/2016 12am to 27/7/2016 12am...
				$time_from = new MongoDB\BSON\UTCDateTime($time_start);

				$time_end = $time_start + $i_sec;

				$time_to = new MongoDB\BSON\UTCDateTime($time_end);

				$query = array();
				$query['timestamp'] = array();
				$query['timestamp']['$lt'] = $time_to;
				$query['timestamp']['$gt'] = $time_from;
				$query['mac'] = $mac;

				$data_arr = $collection->find($query)->toArray();

				$pause = 0;
				$no_data = 0;
				$down = 0;
				$danger = 0;
				$warning = 0;
				$ok = 0;

				//populate the 4 groupings
				foreach ($data_arr as $data){
					$status = $data['status'];

					switch ($status) {
						case "pause":
							$pause++;
							break;
						case "no data":
							$no_data++;
							break;
						case "down":
							$down++;
							break;
						case "danger":
							$danger++;
							break;
						case "warning":
							$warning++;
							break;
						case "ok":
							$ok++;
							break;
					}
				}

				$mode = "others";
				$mode_value = 0;

				if ($pause > $mode_value){
					$mode = "pause";
					$mode_value = $pause;
				}

				if ($no_data > $mode_value){
					$mode = "no data";
					$mode_value = $pause;
				}

				if ($down > $mode_value){
					$mode = "down";
					$mode_value = $down;
				}

				if ($danger > $mode_value){
					$mode = "danger";
					$mode_value = $danger;
				}

				if ($warning > $mode_value){
					$mode = "warning";
					$mode_value = $warning;
				}

				if ($ok > $mode_value){
					$mode = "ok";
					$mode_value = $ok;
				}

				$datetime = $time_from->toDateTime()->setTimezone(new DateTimeZone('Singapore'));
				$datetime_string = $datetime->format('Y-m-d H:i:s');

				$block_result["timestamp"] = $datetime_string;
				$block_result["status"] = $mode;

				array_push($mode_arr, $block_result);
				
				$time_start = $time_end;
			}
			
			$tobeInserted = array();
			$tobeInserted['mac'] = $mac;
			$tobeInserted['date'] = $UTCDT_daystart;
			$tobeInserted['interval'] = strval($i);
			$tobeInserted['data'] = $mode_arr;
			
			$uptime_caching->insertOne($tobeInserted);
				
				#echo "data inserted\n";
				
				#$day_start += $one_day;
			#}
		}
		#echo "end\n";
	}
}
?>
