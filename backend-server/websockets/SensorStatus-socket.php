<?php
require '../vendor/autoload.php';
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

require 'SensorStatus.php';

// Run the server application through the WebSocket protocol on port 9000
//$app = new Ratchet\App("opsdev.sence.io", 9010, '0.0.0.0');
$app = new Ratchet\App("119.81.104.46", 9010, '0.0.0.0');

$app->route('/SensorStatus', new SensorStatus, array('*'));

$app->run();
