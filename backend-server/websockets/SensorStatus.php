<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

require '../vendor/autoload.php';
require '../websocket-functions/common/common-functions.php';

include("sensor-status.php");

class SensorStatus implements MessageComponentInterface {
    public $clients;
    private $logs;
    private $connectedUsers;
    private $connectedUsersNames;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->logs = [];
        $this->connectedUsers = [];
        $this->connectedUsersNames = [];
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
        //$conn->send(json_encode($this->logs));
        $this->connectedUsers [$conn->resourceId] = $conn;
    }

    public function onMessage(ConnectionInterface $from, $msg) {

        echo "received: ".$msg."\n";

        try {

            $output = json_encode(getSensorInfo($msg));

            echo "output! ". $output."\n";

            // switch ($msg) {
            //     case 'OVERALL':
            //         $output = json_encode(collectInfo());
            //         break;
            //     case 'ALL_SENSOR_DATA':
            //         $output = json_encode(retrieveAllSensorInfo());
            //         break;
            //     case 'ALL_SENSOR_DATA_REGIONMALL':
            //         $output = json_encode(retrieveAllSensorInfoRegionMall());
            //         break;
            //     default:
            //         $errorArr = array();
            //         $errorArr['status'] = 502;
            //
            //         $output = json_encode($errorArr);
            //         break;
            // }

        } catch (Exception $e) {
            $errorArr = array(
                "status" => 502,
                "error" => "something's dead"
            );

            $output = json_encode($errorArr);

        } finally {
            echo "sending: ".$output."\n";
            $from->send($output);
        }

    }

    public function onClose(ConnectionInterface $conn) {
        // Detatch everything from everywhere
        echo "Connection lost! ({$conn->resourceId})\n";
        $this->clients->detach($conn);
        unset($this->connectedUsersNames[$conn->resourceId]);
        unset($this->connectedUsers[$conn->resourceId]);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }

}
