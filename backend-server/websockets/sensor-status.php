<?php

require "../vendor/autoload.php";
// include("../websocket-functions/common/common-functions.php");

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

function getSensorInfo($mac){
	global $client;

	//loading test table
	$collection = $client->fluent->sensor_data;
	$sensor_matrix = $client->fluent->sensor_matrix->findOne(['MAC'=>$mac]);

    //loading sensor statuses
    $sensorStatuses = $client->fluent->sensor_current_status->findOne([]);

	$isServer = false;
	$result = array();

	if (strlen($mac) != 17){
		$result['status'] = 200;
		$result['error'] = "shitty mac address";

		return $result;
	}

	$sensor_info = $collection->find(['MAC' => $mac], ['limit' => 1, 'sort'=> ['time' => -1]]);

	$sensor_arr = $sensor_info->toArray();

	if (count($sensor_matrix) == 0) {

		//check if a server with this mac_address exists
		$sensor_matrix = $client->fluent->server_matrix->findOne(['MAC'=>$mac]);

		if (count($sensor_matrix) == 0) {
			$result['status'] = 200;
			$result['error'] = "no such mac address";
			return $result;
		}

		$sensor_info = $client->fluent->server_data->find(['MAC' => $mac], ['limit' => 1, 'sort'=> ['time' => -1]]);

		$sensor_arr = $sensor_info->toArray();

		$isServer = true;
	}

	$result['geo_region'] = $sensor_matrix['geo-region'];
	$result['building'] = $sensor_matrix['building'];

    if (isset($sensor_matrix["thresholds"])) {
        $result["thresholds"] = $sensor_matrix["thresholds"];
    } else {
        $result["thresholds"] = (object) null;
    }

	// name of field is different if it's a server vs sensor
	if ($isServer) {
		$result['sensor_location_level'] = $sensor_matrix['group'];
		$result['sensor_location_id'] = $sensor_matrix['id'];
	} else {
		$result['sensor_location_level'] = $sensor_matrix['sensor-location-level'];
		$result['sensor_location_id'] = $sensor_matrix['sensor-location-id'];

		$result['watchlist'] = $sensor_matrix['watchlist'];
		$result['port'] = $sensor_matrix['port'];
	}

	$result['reboot_available'] = false;

	//check if port is open
	if ($result['port'] != "") {
		$output = exec("netstat -ln | grep ':".$sensor_matrix['port']." ' | grep 'LISTEN'");
		$result['reboot_available'] = ($output != "");
	}

	if (count($sensor_arr) == 0) {
        //check archived data
        if ($isServer){
            $collection = $client->fluent->server_data_archive;
        } else {
            $collection = $client->fluent->sensor_data_archive;
        }

        $archive_info = $collection->find(['MAC' => $mac], ['limit' => 1, 'sort'=> ['time' => -1]]);
        $archive_info_array = $archive_info->toArray();

        if (count($archive_info_array) > 0) {
            $sensor = $archive_info_array[0];

        	$timestamp_time = $sensor['time']->toDateTime()->getTimestamp();//in seconds //->setTimezone(new DateTimeZone("Singapore"));

			//echo $timestamp_time."\n";

        	$date_format = 'd-m-Y H:i:s';

            $result['status'] = $sensorStatuses['sensors'][$mac]['currentStatus'];

            $uptime = floor($sensor['Uptime']);

			//echo $uptime."\n";

        	$last_reboot_time = $timestamp_time - $uptime;

			//echo $last_reboot_time."\n";

			$last_reboot_datetime = new DateTime("@$last_reboot_time");

        	$last_reboot = $last_reboot_datetime->setTimezone(new DateTimeZone('Singapore'));

        	$result['last_reboot'] = $last_reboot->format($date_format); //last reboot time in format

            $result['uptime_percentage'] = 0.0;

            $temp = $sensor['Temperature'];

        	$result['temperature'] = $temp;

        	$CPU = $sensor['CPU'];

        	$result['cpu'] = $CPU;

        	$storage = $sensor['Disk_Space_Used']/$sensor['Disk_Space_Total'] * 100;

        	$result['storage'] = round($storage, 2, PHP_ROUND_HALF_UP);

        	$ram = 100 - ($sensor['RAM_Available']/$sensor['RAM_Total'] * 100);

        	$result['ram'] = round($ram, 2, PHP_ROUND_HALF_UP);

        	$flapping = $sensorStatuses['sensors'][$mac]['flappingStatus'];

        	$result['flapping'] = $flapping;

            $datetime = $sensor['time']->toDateTime()->setTimezone(new DateTimeZone("Singapore"));
            $result['latest_timestamp'] = $datetime->format('d-m-Y H:i:s');


        	$top_5_processes = array();
        	for ($i = 1; $i <= 5; $i++){
        		$top_5_processes[$i] = array();
        	}

        	$top_5_processes[1]['process'] = $sensor['top_process']['process_1']['process_name'];
        	$top_5_processes[1]['usage'] = $sensor['top_process']['process_1']['CPU_usage'];

        	$top_5_processes[2]['process'] = $sensor['top_process']['process_2']['process_name'];
        	$top_5_processes[2]['usage'] = $sensor['top_process']['process_2']['CPU_usage'];

        	$top_5_processes[3]['process'] = $sensor['top_process']['process_3']['process_name'];
        	$top_5_processes[3]['usage'] = $sensor['top_process']['process_3']['CPU_usage'];

        	$top_5_processes[4]['process'] = $sensor['top_process']['process_4']['process_name'];
        	$top_5_processes[4]['usage'] = $sensor['top_process']['process_4']['CPU_usage'];

        	$top_5_processes[5]['process'] = $sensor['top_process']['process_5']['process_name'];
        	$top_5_processes[5]['usage'] = $sensor['top_process']['process_5']['CPU_usage'];

        	$result['top_5_processes'] = $top_5_processes;

            if (isset($sensor['router_latency'])) {
                $result['router_latency'] = $sensor['router_latency'];
            } else {
                $result['router_latency'] = "-";
            }

            if ($isServer) {
                $result['diagnosis'] = "nil";
            } else {
        	    $result['diagnosis'] = $sensorStatuses['sensors'][$mac]['diagnosis'];
            }

        } else {
    		$result['status'] = 200;
    		$result['error'] = "no data";
        }

	} else {
        //Array of the sensor data
    	$sensor = $sensor_arr[0];

		$currentStatus = $sensorStatuses['sensors'][$mac]['currentStatus'];

    	$timestamp_time = $sensor['time']->toDateTime()->getTimestamp();//in seconds //->setTimezone(new DateTimeZone("Singapore"));

		//echo $timestamp_time."\n";

		$date_format = 'd-m-Y H:i:s';

		$result['status'] = $currentStatus;

		$uptime = floor($sensor['Uptime']);

		//echo $uptime."\n";

		$last_reboot_time = $timestamp_time - $uptime;

		//echo $last_reboot_time."\n";

    	$last_reboot_datetime = new DateTime("@$last_reboot_time");

		$last_reboot = $last_reboot_datetime->setTimezone(new DateTimeZone('Singapore'));

    	$result['last_reboot'] = $last_reboot->format($date_format); //last reboot time in format

    	//In %
    	$uptime_percentage = check_Uptime($mac, $isServer); //In %

    	$result['uptime_percentage'] = round(($uptime_percentage * 100), 2, PHP_ROUND_HALF_UP);

    	$temp = $sensor['Temperature'];

    	$result['temperature'] = $temp;

    	$CPU = $sensor['CPU'];

    	$result['cpu'] = $CPU;

    	$storage = $sensor['Disk_Space_Used']/$sensor['Disk_Space_Total'] * 100;

    	$result['storage'] = round($storage, 2, PHP_ROUND_HALF_UP);

    	$ram = 100 - ($sensor['RAM_Available']/$sensor['RAM_Total'] * 100);

    	$result['ram'] = round($ram, 2, PHP_ROUND_HALF_UP);

    	$flapping = $sensorStatuses['sensors'][$mac]['flappingStatus'];

    	$result['flapping'] = $flapping;

        $datetime = $sensor['time']->toDateTime()->setTimezone(new DateTimeZone("Singapore"));
        $result['latest_timestamp'] = $datetime->format('d-m-Y H:i:s');


    	$top_5_processes = array();
    	for ($i = 1; $i <= 5; $i++){
    		$top_5_processes[$i] = array();
    	}

    	$top_5_processes[1]['process'] = $sensor['top_process']['process_1']['process_name'];
    	$top_5_processes[1]['usage'] = $sensor['top_process']['process_1']['CPU_usage'];

    	$top_5_processes[2]['process'] = $sensor['top_process']['process_2']['process_name'];
    	$top_5_processes[2]['usage'] = $sensor['top_process']['process_2']['CPU_usage'];

    	$top_5_processes[3]['process'] = $sensor['top_process']['process_3']['process_name'];
    	$top_5_processes[3]['usage'] = $sensor['top_process']['process_3']['CPU_usage'];

    	$top_5_processes[4]['process'] = $sensor['top_process']['process_4']['process_name'];
    	$top_5_processes[4]['usage'] = $sensor['top_process']['process_4']['CPU_usage'];

    	$top_5_processes[5]['process'] = $sensor['top_process']['process_5']['process_name'];
    	$top_5_processes[5]['usage'] = $sensor['top_process']['process_5']['CPU_usage'];

    	$result['top_5_processes'] = $top_5_processes;

        if (isset($sensor['router_latency'])) {
            $result['router_latency'] = $sensor['router_latency'];
        } else {
            $result['router_latency'] = "-";
        }

        if ($isServer) {
            $result['diagnosis'] = "nil";
        } else {
		   $result['diagnosis'] = $sensorStatuses['sensors'][$mac]['diagnosis'];
        }
	}

    // am I alive?
	$amIAlive = $client->fluent->up_status->findOne(['MAC'=>$mac], ['sort'=> ['time' => -1]]);
    $result['am_i_alive'] = false;
    if ($amIAlive != NULL) {
    	$unix_time_20 = time() - 20;
    	$time_20 = new DateTime("@$unix_time_20", new DateTimeZone("Singapore"));

        $datetime = $amIAlive['time']->toDateTime();
        if(!($time_20 > $datetime)) {
            $result['am_i_alive'] = true;
        }
    }

	return $result;
}
