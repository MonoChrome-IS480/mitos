<?php

require '../../vendor/autoload.php';
// include_once('../common/common-functions.php');

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

//Return useful information of all sensors.
function retrieveAllSensorInfo(){
	global $client;

	$collection = $client->fluent->sensor_data;

	//loading sensor_matrix table
	$sensors = $client->fluent->sensor_matrix->find([]);
	$sensor_arr = $sensors->toArray();

    //loading sensor statuses
    $sensorStatuses = $client->fluent->sensor_current_status->findOne([]);

	$result = array();

	//$sensor -> object for the sensor info
	foreach ($sensor_arr as $sensor) {
		$mac = $sensor['MAC'];

		$result[$mac] = array();

		$result[$mac]['geo-region'] = $sensor['geo-region'];
		$result[$mac]['building'] = $sensor['building'];
		$result[$mac]['sensor-location-level'] = $sensor['sensor-location-level'];
		$result[$mac]['sensor-location-id'] = $sensor['sensor-location-id'];

		$result[$mac]['sensor_type'] = "default";
		$result[$mac]['watchlist'] = $sensor['watchlist'];

        if (isset($sensor["thresholds"])) {
            $result[$mac]["thresholds"] = $sensor["thresholds"];
        } else {
            $result[$mac]["thresholds"] = (object) null;
        }

	    // $sensorStatus = checkStatus($mac);
        $sensorStatus = $sensorStatuses['sensors'][$mac]['currentStatus'];

		if ($sensorStatus == "no data") {
			$result[$mac]["error"] = "no data";
		} else {
            $sensor_info = $collection->find(['MAC' => $mac], ['limit' => 1, 'sort'=> ['time' => -1]]);

    		$sensor_info_arr = $sensor_info->toArray();

            if (count($sensor_info_arr) == 0) {

                $result[$mac]['latest_timestamp'] = "-";
                $result[$mac]['current_status'] = "down";
                $result[$mac]['sensor_status'] = $sensorStatus;
                $result[$mac]['flapping'] = false;

    		    $result[$mac]['network_router'] = "default";

    		    $result[$mac]['temperature'] = "-";
    		    $result[$mac]['CPU_Usage'] = "-";

    		    //For RAM in absolute numbers
    		    $result[$mac]['RAM_available'] = "-";
    		    $result[$mac]['RAM_total'] = "-";
    		    $result[$mac]['RAM_used'] = "-";
    		    $result[$mac]['RAM_free'] = "-";

    		    //For Storage in absolute numbers
    		    $result[$mac]['Disk_Space_total'] = "-";
    		    $result[$mac]['Disk_Space_used'] = "-";
    		    $result[$mac]['Disk_Space_free'] = "-";

            } else {

                $sensor_key = $sensor_info_arr[0];

    		    $datetime = $sensor_key['time']->toDateTime()->setTimezone(new DateTimeZone("Singapore"));

    		    $result[$mac]['latest_timestamp'] = $datetime->format('d-m-Y H:i:s');

    		    if ($sensorStatus != "down"){
    			    $result[$mac]['current_status'] = "up";
    		    } else {
    			    $result[$mac]['current_status'] = "down";
    		    }

    		    $result[$mac]['sensor_status'] = $sensorStatus;

    		    $result[$mac]['flapping'] = $sensorStatuses['sensors'][$mac]['flappingStatus'];

    		    $result[$mac]['network_router'] = "default";

    		    $result[$mac]['temperature'] = $sensor_key['Temperature'];
    		    $result[$mac]['CPU_Usage'] = $sensor_key['CPU']*100;

    		    //For RAM in absolute numbers
    		    $result[$mac]['RAM_available'] = $sensor_key['RAM_Available'];
    		    $result[$mac]['RAM_total'] = $sensor_key['RAM_Total'];
    		    $result[$mac]['RAM_used'] = $sensor_key['RAM_Used'];
    		    $result[$mac]['RAM_free'] = $sensor_key['RAM_Free'];

    		    //For Storage in absolute numbers
    		    $result[$mac]['Disk_Space_total'] = $sensor_key['Disk_Space_Total'];
    		    $result[$mac]['Disk_Space_used'] = $sensor_key['Disk_Space_Used'];
    		    $result[$mac]['Disk_Space_free'] = $sensor_key['Disk_Space_Free'];
            }

        }

	}

	return $result;
}

echo json_encode(retrieveAllSensorInfo());
