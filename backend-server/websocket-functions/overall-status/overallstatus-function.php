<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

require '../../vendor/autoload.php';
// include_once('../common/common-functions.php');


$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

function collectInfo() {
    global $client;

    //$finalResults = array(
    //    "status" => 502
    //);


    //final result to be returned
    $results = array();

    try {

        //loading test table
        $collection = $client->fluent->sensor_data;

        //loading sensor_matrix table
        $sensors = $client->fluent->sensor_matrix;

        //all distinct buildings with sensors
        $buildings = $sensors->distinct('building');

        sort($buildings);

        $finalResults = array();
    	  $finalResults['status'] = 200;

        foreach ($buildings as $building) {
        	// all sensors in the building
        	$all_building_sensors = $sensors->find(['building'=>$building])->toArray();

        	$results[$building] = generateOutputArray($all_building_sensors);
        }

        //$finalResults['data'] = $results;

    } catch (Exception $e) { }

    return $results;

}

function generateOutputArray($sensor_arr) {
    global $client;

    //loading sensor statuses
    $sensorStatuses = $client->fluent->sensor_current_status->findOne([]);

	//store number of sensors in that particular coding
    $noData = 0;
    $paused = 0;
	$down = 0;
	$danger = 0;
	$warning = 0;
	$ok = 0;

	//store mac addresses of sensors that falls onto each particular category
	$noData_arr = array();
    $paused_arr = array();
	$down_arr = array();
	$danger_arr = array();
	$warning_arr = array();
	$ok_arr = array();

	//for each MAC address, get all the data from test collection regarding the MAC address
	foreach ($sensor_arr as $sensor) {
		$mac = $sensor['MAC'];

		$status = $sensorStatuses['sensors'][$mac]['currentStatus'];

		switch ($status) {
            case 'no data':
				$noData++;
				array_push($noData_arr, $mac);

				break;
			case 'down':
				$down++;
				array_push($down_arr, $mac);

				break;
			case 'danger':
				$danger++;
				array_push($danger_arr, $mac);

				break;
			case 'warning':
				$warning++;
				array_push($warning_arr, $mac);

				break;
            case 'paused':
				$paused++;
				array_push($paused_arr, $mac);

                break;
			default:
				$ok++;
				array_push($ok_arr, $mac);

				break;
		}
	}

	//creating arrays and populating with data that is meant to be returned for each colour coding (count, mac address of sensors)
	$noData_final = array();
	$noData_final['count'] = $noData;
	$noData_final['mac_addresses'] = $noData_arr;

	$danger_final = array();
	$danger_final['count'] = $danger;
	$danger_final['mac_addresses'] = $danger_arr;

	$down_final = array();
	$down_final['count'] = $down;
	$down_final['mac_addresses'] = $down_arr;

	$warning_final = array();
	$warning_final['count'] = $warning;
	$warning_final['mac_addresses'] = $warning_arr;

	$ok_final = array();
	$ok_final['count'] = $ok;
	$ok_final['mac_addresses'] = $ok_arr;


	$paused_final = array();
	$paused_final['count'] = $paused;
	$paused_final['mac_addresses'] = $paused_arr;

	//creating and populating the final result to be returned
	$colourResult = array();

	$colourResult['no data'] = $noData_final;
	$colourResult['down'] = $down_final;
	$colourResult['danger'] = $danger_final;
	$colourResult['warning'] = $warning_final;
	$colourResult['paused'] = $paused_final;
	$colourResult['ok'] = $ok_final;

	return $colourResult;
}

echo json_encode(collectInfo());
