<?php

require '../../vendor/autoload.php';
include_once('../common/common-functions.php');
// require '../../vendor/autoload.php';
// include_once('../../websocket-functions/common/common-functions.php');

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

$servers = $client->fluent->server_matrix;

function generateServerHealthOverview() {
    global $collection, $servers;

    $output = array();

    $allServerArr = $servers->find()->toArray();


    $uniqueBuildings = array_unique(array_map(function ($i) { return $i['building']; }, $allServerArr));

    foreach($uniqueBuildings as $building){
        $output[$building] = array();
    }


    foreach($allServerArr as $server) {
        $mac = $server["MAC"];

	    $serverStatus = checkStatus($mac, true);
        $serArr = array(
            "mac" => $mac,
            "cluster" => $server["building"],
            "group" => $server["group"],
            "id" => $server["id"],
            "status" => $serverStatus
        );

        //create array for that group if it doesn't exist
        if (!array_key_exists($server["group"], $output[$server["building"]])) {
            $output[$server["building"]][$server["group"]] = array();
        }

        array_push($output[$server["building"]][$server["group"]], $serArr);

    }

    return $output;
}

echo json_encode(generateServerHealthOverview());
