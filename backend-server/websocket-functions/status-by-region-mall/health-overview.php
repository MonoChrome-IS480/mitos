<?php

require '../../vendor/autoload.php';
// include_once('../common/common-functions.php');
// require '../../vendor/autoload.php';
// include_once('../../websocket-functions/common/common-functions.php');

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

function generatePiHealthOverview() {
    global $client;

    $sensors = $client->fluent->sensor_matrix;
    $allSensorArr = $sensors->find()->toArray();

    $output = array();

    //loading sensor statuses
    $sensorStatuses = $client->fluent->sensor_current_status->findOne([]);

    $uniqueBuildings = array_unique(array_map(function ($i) { return $i['building']; }, $allSensorArr));

    sort($uniqueBuildings);

    foreach($uniqueBuildings as $building){
        //extract snmp speed test reults
        $uploadSpeed = "-";
        $downloadSpeed = "-";
        $systemName = "-";
        $routerModel = "-";
        $testerSensor = "-";
        $speedTestTime = "-";
        $routerUptime = "-";
        $routerStorageTotal = "-";
        $routerStorageUsed = "-";
        $routerMemoryTotal = "-";
        $routerMemoryUsed = "-";
        $routerCPUFreq = "-";
        $routerCPULoad = "-";
        $routerProcessorTemp = "-";

        $currentInterval = "-";

        $speed_test_collection = $client->fluent->snmp_speed_test;
        $result_info = $speed_test_collection->findOne(["building" => $building], ['sort'=> ['time' => -1]]);

        if ($result_info != NULL) {
            $uploadSpeed = $result_info['upload'];
            $downloadSpeed = $result_info['download'];
            $testerSensor = $result_info['MAC'];
            $speedTestTime = $result_info['time']->toDateTime()->setTimezone(new DateTimeZone("Singapore"))->format('d-m-Y H:i:s');
            $systemName = $result_info['systemName'];
            $routerModel = $result_info['model'];
            $routerUptime = $result_info['uptime'];
            $routerStorageTotal = $result_info['storageTotal'];
            $routerStorageUsed = $result_info['storageUsed'];
            $routerMemoryTotal = $result_info['memoryTotal'];
            $routerMemoryUsed = $result_info['memoryUsed'];
            $routerCPUFreq = $result_info['cpuFreq'];
            $routerCPULoad = $result_info['cpuLoad'];
            $routerProcessorTemp = $result_info['processorTemp'];

            $currentInterval = $result_info['current_interval'];
        }

        //initiate array for building
        $output[$building] = array(
            "sensors" => array(),
            "geo_region" => "",
            "level_names" => array(),
            "area_names" => array(),
            "sensor_count" => 0,
            "snmp_speed_test" => array(
                "sensor" => $testerSensor,
                "system_name" => $systemName,
                "model" => $routerModel,
                "upload_speed" => $uploadSpeed,
                "download_speed" => $downloadSpeed,
                "time" => $speedTestTime,
                "current_interval" => $currentInterval,
                "uptime" => $routerUptime,
                "storage_total" => $routerStorageTotal,
                "storage_used" => $routerStorageUsed,
                "memory_total" => $routerMemoryTotal,
                "memory_used" => $routerMemoryUsed,
                "cpu_freq" => $routerCPUFreq,
                "cpu_load" => $routerCPULoad,
                "processor_temp" => $routerProcessorTemp
            )
        );
    }


    foreach($allSensorArr as $sensor) {
        $mac = $sensor["MAC"];

	    $sensorStatus = $sensorStatuses['sensors'][$mac]['currentStatus'];
        $senArr = array(
            "mac" => $mac,
            "region" => $sensor["geo-region"],
            "building" => $sensor["building"],
            "level" => $sensor["sensor-location-level"],
            "id" => $sensor["sensor-location-id"],
            "watchlist" => $sensor["watchlist"],
            "port" => $sensor["port"],
            "status" => $sensorStatus
        );

        if (isset($sensor["thresholds"])) {
            $senArr["thresholds"] = $sensor["thresholds"];
        } else {
            $senArr["thresholds"] = (object) null;
        }

        $senArr['reboot_available'] = false;

    	//check if port is open
    	if ($senArr['port'] != "") {
    		$outputCmd = exec("netstat -ln | grep ':".$senArr['port']." ' | grep 'LISTEN'");
    		$senArr['reboot_available'] = ($outputCmd != "");
    	}

        if ($sensorStatus != "down" && $sensorStatus != "no data"){
            $senArr["current"] = "up";
        } else {
            $senArr["current"] = "down";
        }

        //create array for that level if it doesn't exist
        if (!array_key_exists($sensor["sensor-location-level"], $output[$sensor["building"]]["sensors"])) {
            $output[$sensor["building"]]["sensors"][$sensor["sensor-location-level"]] = array();
        }

        //assign geo-region if it's not been assigned yet
        if ($output[$sensor["building"]]["geo_region"] == "") {
            $output[$sensor["building"]]["geo_region"] = $sensor["geo-region"];
        }

        //add level name if it doesn't exist yet
        if (!in_array($sensor["sensor-location-level"],$output[$sensor["building"]]["level_names"])) {
            array_push($output[$sensor["building"]]["level_names"], $sensor["sensor-location-level"]);
        }

        //add area name if it doesn't exist yet
        if (!in_array($sensor["sensor-location-id"],$output[$sensor["building"]]["area_names"])) {
            array_push($output[$sensor["building"]]["area_names"], $sensor["sensor-location-id"]);
        }

        array_push($output[$sensor["building"]]["sensors"][$sensor["sensor-location-level"]], $senArr);
        $output[$sensor["building"]]["sensor_count"] += 1;

    }

    foreach ($output as $buildingName => $building) {
        $levels = $building["sensors"];
        foreach ($levels as $levelName => $sensorsOnThisLevel) {
            $ids = array();
            foreach ($sensorsOnThisLevel as $key => $row) {
                $ids[$key] = $row['id'];
            }

            array_multisort($ids, SORT_ASC, $sensorsOnThisLevel);

            $output[$buildingName]["sensors"][$levelName] = $sensorsOnThisLevel;
        }
    }

    foreach ($output as $key => $buildingStuff) {
        usort($buildingStuff["level_names"], 'customSort');
        sort($buildingStuff["area_names"]);
        //var_dump($buildingStuff["area_names"]);

        $output[$key]["level_names"] = $buildingStuff["level_names"];
        $output[$key]["area_names"] = $buildingStuff["area_names"];
    }

    return $output;

}

function customSort($a, $b){
    if(is_numeric($a) && !is_numeric($b))
        return -1;
    else if(!is_numeric($a) && is_numeric($b))
        return 1;
    else
        return ($a < $b) ? 1 : -1;
}

echo json_encode(generatePiHealthOverview());
