<?php

require_once '../../vendor/autoload.php';
include_once('../common/common-functions.php');

echo json_encode(generateNotificationUpdate());

function generateNotificationUpdate(){
    $client = new MongoDB\Client("mongodb://127.0.0.1:27017");

    //loading test table
    $collection = $client->fluent->sensor_data;

    //loading sensor_matrix table
    $sensors = $client->fluent->sensor_matrix->find([]);
    $sensor_arr = $sensors->toArray();

    //loading sensor statuses
    $sensorStatuses = $client->fluent->sensor_current_status->findOne([]);

    $notifications_cursor = $client->fluent->notifications;

	//store new notifications to be returned to the client....
	$notifications_result = array();

	//for every sensor...
	foreach ($sensor_arr as $sensor) {
		//Consider displaying sensor in terms of sensor id as compared to mac address.....
		$mac = $sensor['MAC'];
		$building = $sensor['building'];
		$level = $sensor['sensor-location-level'];
		$id = $sensor['sensor-location-id'];

		#$status = checkStatus($mac);
        $status = $sensorStatuses['sensors'][$mac]['currentStatus'];

		if($status == "no data" || $status == "paused"){
			//skip to next record
			continue;
		}

		// $flapping = checkIfFlapping($mac);
        $flapping = $sensorStatuses['sensors'][$mac]['flappingStatus'];

		//flapping
		if ($flapping == "true"){

			//Get last notification record from the database that belongs to this mac address
			$toBeChecked = $notifications_cursor->findOne(['mac'=>$mac], ['sort'=> ['timestamp' => -1]]);

			if ($toBeChecked['problem']['status'] != "Flapping"){

				$tobeInserted = array();
				$tobeInserted['mac'] = $mac;
				$tobeInserted['building'] = $building;
				$tobeInserted['level'] = $level;
				$tobeInserted['id'] = $id;

				$tobeInserted['problem'] = array();
				$tobeInserted['problem']['status'] = "Flapping";
				$tobeInserted['problem']['diagnosis'] = array();

				$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));

				$tobeInserted['timestamp'] = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000); //convert ms to s

				$notifications_cursor->insertOne($tobeInserted);
				
				//change timestamp to datetime object
				$tobeInserted['timestamp'] = $curDate;

				array_push($notifications_result, $tobeInserted);
			}

			//skip to next record
			continue;
		} elseif ($flapping == "abnormal"){
			//Get last notification record from the database that belongs to this mac address
			$toBeChecked = $notifications_cursor->findOne(['mac'=>$mac], ['sort'=> ['timestamp' => -1]]);

			if ($toBeChecked['problem']['status'] != "Abnormal"){

				$tobeInserted = array();
				$tobeInserted['mac'] = $mac;
				$tobeInserted['building'] = $building;
				$tobeInserted['level'] = $level;
				$tobeInserted['id'] = $id;

				$tobeInserted['problem'] = array();
				$tobeInserted['problem']['status'] = "Abnormal";
				$tobeInserted['problem']['diagnosis'] = array();

				$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));

				$tobeInserted['timestamp'] = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000); //convert ms to s

				$notifications_cursor->insertOne($tobeInserted);

				//change timestamp to datetime object
				$tobeInserted['timestamp'] = $curDate;

				array_push($notifications_result, $tobeInserted);
			}

			//skip to next record
			continue;
		} else {
			//Get last notification record from the database that belongs to this mac address
			$toBeChecked = $notifications_cursor->findOne(['mac'=>$mac], ['sort'=> ['timestamp' => -1]]);

			if ($toBeChecked['problem']['status'] == "Flapping" || $toBeChecked['problem']['status'] == "Abnormal"){

				$tobeInserted = array();
				$tobeInserted['mac'] = $mac;
				$tobeInserted['building'] = $building;
				$tobeInserted['level'] = $level;
				$tobeInserted['id'] = $id;

				$tobeInserted['problem'] = array();
				$tobeInserted['problem']['status'] = "No longer Flapping";
				$tobeInserted['problem']['diagnosis'] = array();

				$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));

				$tobeInserted['timestamp'] = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000); //convert ms to s

				$notifications_cursor->insertOne($tobeInserted);

				//change timestamp to datetime object
				$tobeInserted['timestamp'] = $curDate;

				array_push($notifications_result, $tobeInserted);
			}
		}

		$toBeChecked_temp = $notifications_cursor->findOne(['mac'=>$mac], ['sort'=> ['timestamp' => -1]]);

        if ($toBeChecked['problem']['status'] != "Flapping"){

            $diagnosis_temp = $sensorStatuses['sensors'][$mac]['diagnosis'];

    		$diagnosis = $diagnosis_temp['result'];

    		$sensor_array = $notifications_cursor->find(['mac'=>$mac])->toArray();

    		$count = count($sensor_array);

    		if ($status == "down") {
    			//store in db
    			$tobeInserted = array();
    			$tobeInserted['mac'] = $mac;
    			$tobeInserted['building'] = $building;
    			$tobeInserted['level'] = $level;
    			$tobeInserted['id'] = $id;
    			$tobeInserted['problem'] = array();
    			$tobeInserted['problem']['status'] = $status;
    			$tobeInserted['problem']['diagnosis'] = array();

    			$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));

    			$tobeInserted['timestamp'] = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000); //convert ms to s

    			if($count == 0){
    				$notifications_cursor->insertOne($tobeInserted);

    				//insert to the array to be returned
    				array_push($notifications_result, $tobeInserted);
    			} else {
    				//check its last entry
    				$toBeChecked = $notifications_cursor->findOne(['mac'=>$mac], ['sort'=> ['timestamp' => -1]]);

    				if($toBeChecked['problem']['status'] != $status){
    					//insert existing record
    					$notifications_cursor->insertOne($tobeInserted);

    					//change timestamp to datetime object
    					$tobeInserted['timestamp'] = $curDate;

    					//insert to the array to be returned
    					array_push($notifications_result, $tobeInserted);
    				}
    			}
    		} elseif ($status == "danger") {

    			//store in db
    			$tobeInserted = array();
    			$tobeInserted['mac'] = $mac;
    			$tobeInserted['building'] = $building;
    			$tobeInserted['level'] = $level;
    			$tobeInserted['id'] = $id;
    			$tobeInserted['problem'] = array();
    			$tobeInserted['problem']['status'] = $status;
    			$tobeInserted['problem']['diagnosis'] = $diagnosis;

    			$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));

    			$tobeInserted['timestamp'] = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000); //convert ms to s

    			if($count == 0){
    				$notifications_cursor->insertOne($tobeInserted);

    				//change timestamp to datetime object
    				$tobeInserted['timestamp'] = $curDate;

    				//insert to the array to be returned
    				array_push($notifications_result, $tobeInserted);
    			} else {
    				$toBeChecked = $notifications_cursor->findOne(['mac'=>$mac], ['sort'=> ['timestamp' => -1]]);
    				// $toBeChecked = $toBeChecked_temp[0];

    				if($toBeChecked['problem']['status'] != $status){
    					//Insert to DB
    					$notifications_cursor->insertOne($tobeInserted);

    					//change timestamp to datetime object
    					$tobeInserted['timestamp'] = $curDate;

    					//insert to the array to be returned
    					array_push($notifications_result, $tobeInserted);
    				} else {
                        if ($toBeChecked['problem']['diagnosis'] == "nil") {
                            $toBeChecked_Diagnosis = [];
                        } else {
                            $toBeChecked_Diagnosis = $toBeChecked['problem']['diagnosis']->getArrayCopy();
                        }

                        $diagonsisArrr = $diagnosis->getArrayCopy();

                        // var_dump($diagonsisArrr);
                        // var_dump($toBeChecked_Diagnosis);

    					$diff = array_diff($toBeChecked_Diagnosis, $diagonsisArrr);

    					//If difference in diagnosis is detected, then insert into DB
    					if (count($diff) > 0) {
    						$notifications_cursor->insertOne($tobeInserted);

    						//change timestamp to datetime object
    						$tobeInserted['timestamp'] = $curDate;

    						//insert to the array to be returned
    						array_push($notifications_result, $tobeInserted);
    					}
    				}
    			}
    		} else {
    			//For sensor = healthy/warning only
    			//prepare to store in db

    			if ($count != 0){
    			//no existing records, no problem. skip this
    				$tobeInserted = array();
    				$tobeInserted['mac'] = $mac;
    				$tobeInserted['building'] = $building;
    				$tobeInserted['level'] = $level;
    				$tobeInserted['id'] = $id;
    				$tobeInserted['problem'] = array();
    				$tobeInserted['problem']['status'] = $status; //can be up
    				$tobeInserted['problem']['diagnosis'] = array();

    				$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));

    				$tobeInserted['timestamp'] = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000); //convert ms to s

    				$toBeChecked = $notifications_cursor->findOne(['mac'=>$mac], ['sort'=> ['timestamp' => -1]]);

    				//check if previous record of sensor is down, if so will push notifications
    				if($toBeChecked['problem']['status'] != $status){
    					//insert existing record
    					$notifications_cursor->insertOne($tobeInserted);

    					//change timestamp to datetime object
    					$tobeInserted['timestamp'] = $curDate;

    					//insert to the array to be returned
    					array_push($notifications_result, $tobeInserted);
    				}
    			}
    		}
    	}

    }
	//If no new notifications, this should return an empty array
	return $notifications_result;
}

?>
