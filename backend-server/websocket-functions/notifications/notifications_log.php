<?php

require '../../vendor/autoload.php';
include_once('../common/common-functions.php');


function getAllNotifications($number_of_records=100){
	$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

	$notifications_cursor = $client->fluent->notifications;

	$endResult = array();

	//limit to $number_of_records records, in desc order by timestamp
	$results = $notifications_cursor->find([], ['limit' => $number_of_records, 'sort' => ['timestamp' => -1]])->toArray();

	foreach ($results as $result) {
		$entry = array(
            "mac" => $result['mac'],
			"buidling" => $result["building"],
			"level" => $result["level"],
			"id" => $result["id"],
    		"problem" => $result['problem'],
    		"timestamp" => $result['timestamp']
        );

        array_push($endResult, $entry);

	}

	// $endResult['data'] = $return;

	return $endResult;
}

$numberOfEntries = 100;
if (isset($_GET["log_count"])) {
    $numberOfEntries = intval($_GET["log_count"]);
}

echo json_encode(getAllNotifications($numberOfEntries));
