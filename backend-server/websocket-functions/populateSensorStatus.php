<?php

require_once '/var/www/html/backend/vendor/autoload.php';
include_once('common/common-functions.php');
// include_once '/var/www/html/backend/api-functions/emailGenerator.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

generateSensorInfo();

function generateSensorInfo() {
    global $client;

    $curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
    $timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

    $result = array(
        "type" => "current_status",
        "time" => $timestamp,
        "sensors" => array()
    );

    //get all sensors
    $sensor_arr = $client->fluent->sensor_matrix->find([])->toArray();

    foreach ($sensor_arr as $sensor) {
        $mac = $sensor['MAC'];

        if (isset($sensor['thresholds'])) {
            $thresholds = $sensor['thresholds'];
        } else {
            $thresholds = array(
                'danger' => array(
                    'disk_usage' => 0.9,
                    'cpu_usage' => 60,
                    'ram_usage' => 0.6,
                    'downtime_percentage' => 0.6,
                    'temperature' => 65,
                ),
                'warning' => array(
                    'disk_usage' => 0.6,
                    'cpu_usage' => 40,
                    'ram_usage' => 0.4,
                    'downtime_percentage' => 0.4,
                    'temperature' => 60,
                )
            );
        }

        $sensor_data = getSensorData($mac, $thresholds);


        $result['sensors'][$mac] = $sensor_data;
    }

    $collection = $client->fluent->sensor_current_status;
    $collection->findOneAndReplace(["type" => "current_status"],$result);

}

function getSensorData($mac, $thresholds) {
    global $client;

    $curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
    $timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

    $result = array(
        "currentStatus" => "no data",
        "diagnosis" => array(
            "result" => "nil",
            "fields" => array()
        ),
        "flappingStatus" => checkIfFlapping($mac)
    );

	$status_collection = $client->fluent->sensor_status;

	$matrix = $client->fluent->sensor_matrix;

	$sensor_info = $matrix->findOne(['MAC'=>$mac]);

	$pause = $sensor_info['pause_status'];

	if ($pause == true){
		$result['currentStatus'] = "paused";

		$tobeInserted = array(
            'mac' => $mac,
            'status' => "paused",
            'timestamp' => $timestamp
        );

		//insert sensor current status into DB with $mac and time.
		$status_collection->insertOne($tobeInserted);

		return $result;
	}

    //begin data generation

    // pulling information from DB
	$collection = $client->fluent->sensor_data;

	$sensor_info = $collection->findOne(
        ['MAC' => $mac],
        ['sort'=> ['time' => -1]]
    );

	if ($sensor_info == NULL) {
        //check archived data
        $collection = $client->fluent->sensor_data_archive;

        $info = $collection->findOne(['MAC' => $mac]);

        if ($info != NULL) {
    		$result['currentStatus'] = "down";
			//no need to check $writeToDB, will always be true
			$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
			$timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

			$tobeInserted = array(
                'mac' => $mac,
                'status' => "down",
                'timestamp' => $timestamp
            );

			//insert sensor current status into DB with $mac and time.
			$status_collection->insertOne($tobeInserted);

            return $result;
        }

        $result['currentStatus'] = "no data";
        //no need to check $writeToDB, will always be true
        $curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
        $timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

        $tobeInserted = array(
            'mac' => $mac,
            'status' => "no data",
            'timestamp' => $timestamp
        );

        //insert sensor current status into DB with $mac and time.
        $status_collection->insertOne($tobeInserted);
        return $result;
	}

	$sensor = $sensor_info;

	$datetime = $sensor['time']->toDateTime();

    if ($mac == "B8:27:EB:05:B0:B0") {
        $myfile = fopen("/home/monochrome/populateSensorStatusTEST.txt", "a");
        $txt = date('Y-m-d H:i:s')." -> ".date_format($datetime, 'Y-m-d H:i:s')."\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }

	//retrieve settings from DB
	$settings = $client->fluent->universal_settings->findOne([]);

	$max_data_gap = $settings['max_data_gap'];

    if ((time() - $datetime->getTimestamp()) > $max_data_gap) {
		$result['currentStatus'] = "down";

        $curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
        $timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

        $tobeInserted = array(
            'mac' => $mac,
            'status' => "down",
            'timestamp' => $timestamp
        );

        //insert sensor current status into DB with $mac and time.
        $status_collection->insertOne($tobeInserted);

        return $result;

	} else {
        //check uptime
        $onehour_milli = 60 * 60 * 1000; //1 hours in millseconds
        $onehour_sec = 60 * 60;

        $timenow = round(microtime(true) * 1000);

        $query_period = new MongoDB\BSON\UTCDateTime($timenow-$onehour_milli);

        $query = array(
            'time' => array(
                '$gt' => $query_period
            ),
            'MAC' => $mac
        );

        $fields = array(
            '_id' => false,
            'time' => true
        );

        $test = $collection->find($query, ["projection" => $fields, "sort" => ["time" => 1]]);

        $test_arr = $test->toArray();
    	$arr_size = count($test_arr);

        $total_uptime = 0;

        //If there is only 1 record, default total up time to be 0
        if ($arr_size > 1){
    	    for ($i = 0; $i < $arr_size-1; $i++) {
    	    	$first = $test_arr[$i]['time']->toDateTime()->getTimestamp();
    	    	$second = $test_arr[$i+1]['time']->toDateTime()->getTimestamp();

    	    	$diff = $second - $first;

    	    	if ($diff < 20){
    	    		$total_uptime += $diff;
    	    	}
    	    }
    	}

        $downtime_percentage = 1 - $total_uptime/$onehour_sec;

    	//round down
        if ($downtime_percentage > 1){
        	$downtime_percentage = 1;
        }

		$temperature = $sensor['Temperature'];

		$disk_usage = $sensor['Disk_Space_Used']/$sensor['Disk_Space_Total'];
		$cpu_usage = $sensor['CPU'];
		$ram_usage = 1 - ($sensor['RAM_Available']/$sensor['RAM_Total']);

        // form status and diagnosis
        $diagnosisResult = array();
        $diagnosisFields = array();
        $result['currentStatus'] = "ok";

        // storage
        if ($disk_usage > $thresholds['warning']['disk_usage']) {
            array_push($diagnosisResult, "Storage overuse");
            array_push($diagnosisFields, "storage");

            $result['currentStatus'] = "warning";
            if ($disk_usage > $thresholds['danger']['disk_usage']) {
                $result['currentStatus'] = "danger";
            }
        }

        // CPU
        if ($cpu_usage > $thresholds['warning']['cpu_usage']) {
            array_push($diagnosisResult, "CPU overuse");
            array_push($diagnosisFields, "cpu");

            if ($result['currentStatus'] != "danger") {
                $result['currentStatus'] = "warning";
            }
            if ($cpu_usage > $thresholds['danger']['cpu_usage']) {
                $result['currentStatus'] = "danger";
            }
        }

        // RAM
        if ($ram_usage > $thresholds['warning']['ram_usage']) {
            array_push($diagnosisResult, "RAM overuse");
            array_push($diagnosisFields, "ram");

            if ($result['currentStatus'] != "danger") {
                $result['currentStatus'] = "warning";
            } if ($ram_usage > $thresholds['danger']['ram_usage']) {
                $result['currentStatus'] = "danger";
            }
        }

        // Temperature
        if ($temperature > $thresholds['warning']['temperature']) {
            array_push($diagnosisResult, "overheating");
            array_push($diagnosisFields, "temperature");

            if ($result['currentStatus'] != "danger") {
                $result['currentStatus'] = "warning";
            } if ($temperature > $thresholds['danger']['temperature']) {
                $result['currentStatus'] = "danger";
            }
        }

        // downtime
		if ($downtime_percentage > $thresholds['warning']['downtime_percentage']) {
			$time_1h = round(microtime(true) * 1000) - (60 * 60 * 1000);
			$datetime_1h = new MongoDB\BSON\UTCDateTime($time_1h);

			//check different conditions
			$query = array(
                'MAC' => $mac,
                'time' => array(
                    'lt' => $datetime_1h
                )
            );

			$last_up_data = $collection->findOne($query);

			if ($last_up_data == NULL){
				$collection_archive = $client->fluent->sensor_data_archive;

				$last_up_data_archive = $collection_archive->findOne(['MAC'=>$mac]);

				if($last_up_data_archive == NULL){
					//just deployed
					array_push($diagnosisResult, "just began operation: warming up");
					array_push($diagnosisFields, "uptime");
				} else {
					//there were records in archive
					array_push($diagnosisResult, "has been down for an extended period");
					array_push($diagnosisFields, "uptime");
				}
			} else {
				array_push($diagnosisResult, "has been down for an extended period");
				array_push($diagnosisFields, "uptime");
			}

            if ($result['currentStatus'] != "danger") {
                $result['currentStatus'] = "warning";
            }
            if ($downtime_percentage > $thresholds['danger']['downtime_percentage']) {
                $result['currentStatus'] = "danger";
            }
		}
    }

    if (count($diagnosisResult) > 0) {
        $result['diagnosis']['result'] = $diagnosisResult;
        $result['diagnosis']['fields'] = $diagnosisFields;
    } else {
        // $result['diagnosis'] = "nil";
    }

    // enter record to DB
	$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
	$timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

	$status_collection = $client->fluent->sensor_status;

	$tobeInserted = array(
        'mac' => $mac,
        'status' => $result['currentStatus'],
        'timestamp' => $timestamp
    );

	//insert sensor current status into DB with $mac and time.
	$status_collection->insertOne($tobeInserted);

    return $result;

}
