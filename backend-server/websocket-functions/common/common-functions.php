<?php
//require_once "../../vendor/autoload.php"; //For testing purposes

// check status of a specified sensor
// returns status as 'healthy', 'warning', 'danger', or 'down'

//Used by Report Generator, Do Not Remove
function checkStatus($mac, $IfServer=false, $sensor=null){
    ini_set('memory_limit', '-1');

	$client = new MongoDB\Client("mongodb://127.0.0.1:27017");
    $thresholds = array(
        'danger' => array(
            'disk_usage' => 0.9,
            'cpu_usage' => 60,
            'ram_usage' => 0.6,
            'downtime_percentage' => 0.6,
            'temperature' => 65,
        ),
        'warning' => array(
            'disk_usage' => 0.6,
            'cpu_usage' => 40,
            'ram_usage' => 0.4,
            'downtime_percentage' => 0.4,
            'temperature' => 60,
        )
    );

	$status_collection = $client->fluent->sensor_status;

	if ($IfServer){
		$status_collection = $client->fluent->server_status;
	}

	
	$matrix = $client->fluent->sensor_matrix;

	$sensor_info = $matrix->findOne(['MAC'=>$mac]);

	$pause = $sensor_info['pause_status'];

	if ($pause == true){
		$tobeInserted = array();
		$tobeInserted['mac'] = $mac;
		$tobeInserted['status'] = "paused";
		$tobeInserted['timestamp'] = $timestamp;

		//insert $sensor_Status into DB with $mac and time.
		$status_collection->insertOne($tobeInserted);

		return "paused";
	}
	

	$writeToDB = false;

	$sensor_Status = "";

	#$unix_time_20 = time() - 20;

	//ensure these 2 records are close to ensure as little discretancy to the data as possible
	#$time_20 = new DateTime("@$unix_time_20", new DateTimeZone("Singapore"));

	//if $sensor_data was not passed in, then extract. Preventing double work of pulling data from DB
	if ($sensor == null){
		//write to DB
		$writeToDB = true;

		$collection = $client->fluent->sensor_data;

		if ($IfServer){
			$collection = $client->fluent->server_data;
		}

		$sensor_info = $collection->find(['MAC' => $mac], ['sort'=> ['time' => -1]]);

		$sensor_array = $sensor_info->toArray();

		if (count($sensor_array) == 0) {
            //check archived data
            $collection = $client->fluent->sensor_data_archive;

			if ($IfServer){
				$collection = $client->fluent->server_data_archive;
			}

            $info = $collection->find(['MAC' => $mac], ['limit' => 1]);
			$info_array = $info->toArray();

            if (count($info_array) > 0) {
        		$sensor_Status = "down";

				//no need to check $writeToDB, will always be true
				$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
				$timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

				$tobeInserted = array();
				$tobeInserted['mac'] = $mac;
				$tobeInserted['status'] = $sensor_Status;
				$tobeInserted['timestamp'] = $timestamp;

				//insert $sensor_Status into DB with $mac and time.
				$status_collection->insertOne($tobeInserted);

                return "down";
            } else {
                return "no data";
            }
		}

		$sensor = $sensor_array[0];
	}

	$unix_time = time();

	//ensure these 2 records are close to ensure as little discretancy to the data as possible
	#$time_20 = new DateTime("@$unix_time_20", new DateTimeZone("Singapore"));

	$datetime = $sensor['time']->toDateTime()->getTimestamp();

	// check if latest record is within 20 seconds from current time. Used to determine whether sensor up or down

	if(($unix_time - $datetime) > 20) {
		$sensor_Status = "down";
	} else {
		//if sensor up

		if($IfServer){ //If checking server
			$downtime_percentage = 1 - check_Uptime($mac, true);
		} else {
			$downtime_percentage = 1 - check_Uptime($mac);
		}

		$temperature = $sensor['Temperature'];

		$disk_usage = $sensor['Disk_Space_Used']/$sensor['Disk_Space_Total'];
		$cpu_usage = $sensor['CPU'];
		$ram_usage = 1 - ($sensor['RAM_Available']/$sensor['RAM_Total']);

		if ($disk_usage > $thresholds['danger']['disk_usage'] || $cpu_usage > $thresholds['danger']['cpu_usage'] || $ram_usage > $thresholds['danger']['ram_usage'] || $downtime_percentage > $thresholds['danger']['downtime_percentage'] || $temperature > $thresholds['danger']['temperature']) {

			$sensor_Status = "danger";

		} elseif ($disk_usage > $thresholds['warning']['disk_usage'] || $cpu_usage > $thresholds['warning']['cpu_usage'] || $ram_usage > $thresholds['warning']['ram_usage'] || $downtime_percentage > $thresholds['warning']['downtime_percentage'] || $temperature > $thresholds['warning']['temperature']) {

			$sensor_Status = "warning";

		} else {

			$sensor_Status = "ok";
		}
	}

	//if $writeToDB is false, skip this block. Directly return $sensor_Status
	if ($writeToDB == true){
		$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
		$timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

		$status_collection = $client->fluent->sensor_status;

		$tobeInserted = array();
		$tobeInserted['mac'] = $mac;
		$tobeInserted['status'] = $sensor_Status;
		$tobeInserted['timestamp'] = $timestamp;

		//insert $sensor_Status into DB with $mac and time.
		$status_collection->insertOne($tobeInserted);
	}

	return $sensor_Status;
}

function checkWhy($mac){
	$client = new MongoDB\Client("mongodb://127.0.0.1:27017");
    $thresholds = array(
        'danger' => array(
            'disk_usage' => 0.9,
            'cpu_usage' => 60,
            'ram_usage' => 0.6,
            'downtime_percentage' => 0.6,
            'temperature' => 65,
        ),
        'warning' => array(
            'disk_usage' => 0.6,
            'cpu_usage' => 40,
            'ram_usage' => 0.4,
            'downtime_percentage' => 0.4,
            'temperature' => 60,
        )
    );

	$collection = $client->fluent->sensor_data;

	$sensor_info = $collection->find(['MAC' => $mac], ['sort'=> ['time' => -1]]);

	$sensor_arr = $sensor_info->toArray();

	$result = array();
	$fields = array();

	foreach($sensor_arr as $sensor) { //should only have 1 time
		$status = checkStatus($mac, false, $sensor);

		$downtime_percentage = 1 - check_Uptime($mac);

		$temperature = $sensor['Temperature'];

		$disk_usage = $sensor['Disk_Space_Used']/$sensor['Disk_Space_Total'];
		$cpu_usage = $sensor['CPU'];
		$ram_usage = 1 - ($sensor['RAM_Available']/$sensor['RAM_Total']);

		if ($status == "warning" || $status == "danger"){

			if ($downtime_percentage > $thresholds['warning']['downtime_percentage']) {
				$time_1h = round(microtime(true) * 1000) - (60 * 60 * 1000);
				$datetime_1h = new MongoDB\BSON\UTCDateTime($time_1h);

				//check different conditions
				$query = array();
				$query['MAC'] = $mac;
				$query['time'] = array();
				$query['time']['lt'] = $datetime_1h;

				$last_up_data = $collection->find($query, ['sort'=> ['time' => 1]])->toArray();

				if (count($last_up_data) == 0){
					$collection_archive = $client->fluent->sensor_data_archive;

					$last_up_data_archive = $collection_archive->find(['MAC'=>$mac])->toArray();

					if(count($last_up_data_archive) == 0){
						//just deployed
						array_push($result, "just started operation, warming up");
						array_push($fields, "uptime");
					} else {
						//there were records in archive
						array_push($result, "it was down for quite some time");
						array_push($fields, "uptime");
					}
				} else {
					array_push($result, "it was down for quite some time");
					array_push($fields, "uptime");
				}
			}

			if ($cpu_usage > $thresholds['warning']['cpu_usage']) {
				array_push($result, "CPU overuse");
				array_push($fields, "cpu");
			}

			if ($ram_usage > $thresholds['warning']['ram_usage']) {
				array_push($result, "RAM overuse");
				array_push($fields, "ram");
			}

			if ($disk_usage > $thresholds['warning']['downtime_percentage']) {
				array_push($result, "Storage overuse");
				array_push($fields, "storage");
			}

			if ($temperature > $thresholds['warning']['temperature']) {
				array_push($result, "Overheating");
				array_push($fields, "temperature");
			}

		} else {
				//You want empty array for no problem or not? If so just comment out the line below.
				array_push($result, "-");
		}

	}

	$output = array(
		"result" => $result,
		"fields" => $fields
	);

	return $output;
}

//This function is to check the uptime % for the sensor with the given mac address for the past 1 hour.
function check_Uptime($mac_add, $IfServer=false){
	$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

	$collection = $client->fluent->sensor_data;

	if ($IfServer){
		$collection = $client->fluent->server_data;
	}

    $onehour_milli = 60 * 60 * 1000; //1 hours in millseconds
    $onehour_sec = 60 * 60;

    $timenow = round(microtime(true) * 1000);

    $query_period = new MongoDB\BSON\UTCDateTime($timenow-$onehour_milli);

    $query = array();
    $query['time'] = array();
    $query['time']['$gt'] = $query_period;
    $query['MAC'] = $mac_add;

    $test = $collection->find($query, ["sort" => ["time" => 1]]);

    $test_arr = $test->toArray();
	$arr_size = count($test_arr);

    $total_uptime = 0;

    //If there is only 1 record, default total up time to be 0
    if ($arr_size > 1){
	    for ($i = 0; $i < $arr_size-1; $i++) {
	    	$first = $test_arr[$i]['time']->toDateTime()->getTimestamp();
	    	$second = $test_arr[$i+1]['time']->toDateTime()->getTimestamp();

	    	$diff = $second - $first;

	    	if ($diff < 20){
	    		$total_uptime += $diff;
	    	}
	    }
	}

    $uptime_percentage = $total_uptime/$onehour_sec;

	//round down
    if ($uptime_percentage > 1){
    	$uptime_percentage = 1;
    }

    return $uptime_percentage;
}

//check if sensor if flapping(up, down, up, down....)
function checkIfFlapping($mac, $duration=10) {
	$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

	$collection = $client->fluent->sensor_data;
	$matrix = $client->fluent->sensor_matrix;
	$settings = $client->fluent->universal_settings->findOne([]);

	$flap_number = (int) $settings['flapping_threshold']; //get flapping threshold from settings DB
	$down_threshold = (int) $settings['max_data_gap']; //get down threshold from settings DB

	$sensor_info = $matrix->findOne(['MAC'=>$mac]);

	$pause = $sensor_info['pause_status'];

	if ($pause == true){
		return "false"; #sensor cannot be flapping when paused
	}

	$duration_milli = $duration * 60 * 1000; //$duration minutes in millseconds

    $timenow = round(microtime(true) * 1000);

    $query_period = new MongoDB\BSON\UTCDateTime($timenow-$duration_milli);

    $query = array();
    $query['time'] = array();
    $query['time']['$gt'] = $query_period;
    $query['MAC'] = $mac;

    $test = $collection->find($query, ["sort" => ["time" => 1]]);

    $test_arr = $test->toArray();

	$arr_size = count($test_arr);

	$down_count = 0;

    //logic for flapping within
    for ($i = 0; $i < $arr_size-1; $i++) {
    	$first = $test_arr[$i]['time']->toDateTime()->getTimestamp();
		$second = $test_arr[$i+1]['time']->toDateTime()->getTimestamp();

		$diff = $second - $first;

		if ($diff > $down_threshold){
			$down_count++;

			if($down_count >= $flap_number){
				//exits for loop and return true

				//sensor flapping, now check if abnormal

				$weighted_avg = 0;

				$ten_min = 10 * 60 * 1000; //10 mins in milliseconds

				//start counting from 50 mins ago.
				$time_start = $timenow - (5*$ten_min);

				for ($block = 1; $block <= 4; $block++){ //from 50 mins until 10 mins ago
					//10 min block

					$local_count = 0; //store down count for each 10 min block

					$time_end = $timenow + $ten_min;

					$abnormal_query = array();
					$abnormal_query['time'] = array();
					$abnormal_query['time']['$lt'] = $time_start;
					$abnormal_query['time']['$gt'] = $time_end;
					$abnormal_query['MAC'] = $mac;

					$data_arr = $collection->find($abnormal_query, ["sort" => ["time" => 1]])->toArray();

					for ($x = 0; $x < count($data_arr)-1; $x++) {
						$first = $data_arr[$x]['time']->toDateTime()->getTimestamp();
						$second = $data_arr[$x+1]['time']->toDateTime()->getTimestamp();

						$diff = $second - $first;

						if ($diff >= 20){
							$local_count++;
						}
					}

					$weighted_avg += ($local_count * $block/10.0); //at block 1, 10%(0.1). block 2, 20% and so on.

					$time_start = $time_end;
				}

				$final_weighted_avg = $weighted_avg + max(1, $weighted_avg * 0.3); //for now, 30% threshold

				if ($down_count > $final_weighted_avg) {
					//flapping is abnormal
					return "abnormal";
				}

				return "true";
			}
		}
    }

    return "false";
}

//checkStatus("B8:27:EB:05:B0:B0");
