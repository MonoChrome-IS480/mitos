define({ "api": [
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Get Buildings",
    "version": "0.1.0",
    "name": "Get",
    "group": "Buildings",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Buildings retrieved successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Buildings retrieved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/get-buildings.php",
    "groupTitle": "Buildings"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Get Building's Sensors",
    "version": "0.1.0",
    "name": "Get_Sensors",
    "group": "Buildings",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "building",
            "description": "<p>Building name of building to have all its sensors retrieved</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Building sensors retrieved successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Building sensors retrieved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingBuilding",
            "description": "<p>Building name is missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/get-sensors-in-building.php",
    "groupTitle": "Buildings"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Get Historical Charts",
    "version": "0.1.0",
    "name": "Historical_Charts",
    "group": "Charts",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mac",
            "description": "<p>The mac address of the sensor that the chart belongs to.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "start_date",
            "description": "<p>The starting period of the chart.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "end_date",
            "description": "<p>The ending period of the chart.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "interval",
            "description": "<p>The interval period between data points on the chart.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "metric",
            "description": "<p>The type of the chart that will be generated.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Chart data retrieved successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Chart data retrieved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some fields are missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/get_historical_chart.php",
    "groupTitle": "Charts"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Get Uptime Charts",
    "version": "0.1.0",
    "name": "Uptime_Charts",
    "group": "Charts",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "building",
            "description": "<p>The building for which to generate charts.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "start_date",
            "description": "<p>The starting period of the chart.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "end_date",
            "description": "<p>The ending period of the chart.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "interval",
            "description": "<p>The interval period between data points on the chart.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Chart data retrieved successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Chart data retrieved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some fields are missing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/get_uptime_chart.php",
    "groupTitle": "Charts"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Get Notifications Log",
    "version": "0.1.0",
    "name": "Notifications_Log",
    "group": "Notifications",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "log_count",
            "description": "<p>The number of past notifications to generate.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Notifcations Log retrieved successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Notifcations Log retrieved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/get_notifications_log.php",
    "groupTitle": "Notifications"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Delete Sensor",
    "version": "0.1.0",
    "name": "Delete",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "MAC",
            "description": "<p>Mac address of the sensor to delete</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Sensor deleted successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensor deleted successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingMAC",
            "description": "<p>Mac address is missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidMAC",
            "description": "<p>The mac address is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid mac address\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/delete-sensor.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Edit Sensor",
    "version": "0.1.0",
    "name": "Edit",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "MAC",
            "description": "<p>Mac address of the sensor to update</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "geo-region",
            "description": "<p>Geographical Region of the sensor to update</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "building",
            "description": "<p>The building name where the sensor to update is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sensor-location-level",
            "description": "<p>The level in the building where the sensor to update is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sensor-location-id",
            "description": "<p>The id of sensor to update is within the building and level</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerDisk",
            "description": "<p>The threshold for Danger status for the sensor in terms of disk usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerCPU",
            "description": "<p>The threshold for Danger status for the sensor in terms of CPU usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerRAM",
            "description": "<p>The threshold for Danger status for the sensor in terms of RAM usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerDTPercentage",
            "description": "<p>The threshold for Danger status for the sensor in terms of downtime percentage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerTemp",
            "description": "<p>The threshold for Danger status for the sensor in terms of Temperature</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningDisk",
            "description": "<p>The threshold for Warning status for the sensor in terms of disk usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningCPU",
            "description": "<p>The threshold for Warning status for the sensor in terms of CPU usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningRAM",
            "description": "<p>The threshold for Warning status for the sensor in terms of RAM usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningDTPercentage",
            "description": "<p>The threshold for Warning status for the sensor in terms of downtime percentage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningTemp",
            "description": "<p>The threshold for Warning status for the sensor in terms of Temperature</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Sensor updated successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensor updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingMAC",
            "description": "<p>Mac address is missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidMAC",
            "description": "<p>The mac address is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidDisk",
            "description": "<p>The Disk threshold is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCPU",
            "description": "<p>The CPU threshold is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidRAM",
            "description": "<p>The RAM threshold is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidTemp",
            "description": "<p>The temperature threshold is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid mac address\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid disk usage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid CPU usage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid RAM usage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid Downtime percentage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid temperature threshold\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/edit-sensor.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Get Sensor",
    "version": "0.1.0",
    "name": "Get",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "MAC",
            "description": "<p>Mac address of the sensor to be retrieved</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Sensor info retrieved successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensor info retrieved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingMAC",
            "description": "<p>Mac address is missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidMAC",
            "description": "<p>The mac address is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid mac address\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/get-sensor-info.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Insert Sensor",
    "version": "0.1.0",
    "name": "Insert",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "MAC",
            "description": "<p>Mac address of the sensor to insert</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "geo-region",
            "description": "<p>Geographical Region of the sensor to insert</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "building",
            "description": "<p>The building name where the sensor to insert is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sensor-location-level",
            "description": "<p>The level in the building where the sensor to insert is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sensor-location-id",
            "description": "<p>The id of sensor to insert is within the building and level</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerDisk",
            "description": "<p>The threshold for Danger status for the sensor in terms of disk usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerCPU",
            "description": "<p>The threshold for Danger status for the sensor in terms of CPU usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerRAM",
            "description": "<p>The threshold for Danger status for the sensor in terms of RAM usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerDTPercentage",
            "description": "<p>The threshold for Danger status for the sensor in terms of downtime percentage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dangerTemp",
            "description": "<p>The threshold for Danger status for the sensor in terms of Temperature</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningDisk",
            "description": "<p>The threshold for Warning status for the sensor in terms of disk usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningCPU",
            "description": "<p>The threshold for Warning status for the sensor in terms of CPU usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningRAM",
            "description": "<p>The threshold for Warning status for the sensor in terms of RAM usage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningDTPercentage",
            "description": "<p>The threshold for Warning status for the sensor in terms of downtime percentage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "warningTemp",
            "description": "<p>The threshold for Warning status for the sensor in terms of Temperature</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Sensor inserted successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensor inserted successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some of the fields are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidMAC",
            "description": "<p>The mac address is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidDisk",
            "description": "<p>The Disk threshold is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCPU",
            "description": "<p>The CPU threshold is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidRAM",
            "description": "<p>The RAM threshold is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidTemp",
            "description": "<p>The temperature threshold is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid mac address\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid disk usage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid CPU usage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid RAM usage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid Downtime percentage threshold\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid temperature threshold\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/insert-new-sensor.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Pin Sensor",
    "version": "0.1.0",
    "name": "Pause",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mac_addresses",
            "description": "<p>Mac Addresses of the sensors to pin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pause_status",
            "description": "<p>True to pin, false to unpin</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Sensors pinned/unpinned successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensors pin/unpinned successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingMACS",
            "description": "<p>Mac addresses are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidMACS",
            "description": "<p>The mac addresses are invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidPin",
            "description": "<p>The pin status is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid mac addresses\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid pin status\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/sensor-watchlist-pin.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Pause Sensor",
    "version": "0.1.0",
    "name": "Pause",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mac_addresses",
            "description": "<p>Mac Addresses of the sensors to pause</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pause_status",
            "description": "<p>True to pause, false to unpause</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Sensors paused successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensors paused successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingMACS",
            "description": "<p>Mac addresses are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidMACS",
            "description": "<p>The mac addresses are invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidPause",
            "description": "<p>The pause status is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid mac addresses\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid pause status\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/sensor-pause.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Reboot Sensor",
    "version": "0.1.0",
    "name": "Reboot",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "MAC",
            "description": "<p>Mac address of the sensor to be rebooted</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>username credentials of the sensor to be rebooted</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password credentials of the sensor to be rebooted</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Sensor rebooted successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensor rebooted successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some Fields are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "EmptyPort",
            "description": "<p>Port Number not configured.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Port Number not configured\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/initialize_reboot_sequence.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Reboot Sensor",
    "version": "0.1.0",
    "name": "Reboot",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mac_addresses",
            "description": "<p>Mac Address of the sensors to be schedule downtime</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of downtime to be scheduled</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "startDate",
            "description": "<p>Start Date of the scheduled downtime</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "endDate",
            "description": "<p>End Date of the scheduled downtime</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Downtime Schedules implemented successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Downtime Schedules implemented successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some fields are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidMACS",
            "description": "<p>Some Mac Address provided are invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidType",
            "description": "<p>Type provided invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid Mac Address(es)\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid Type\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/insert-new-downtime-event.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Set Sensor Speed Test Interval",
    "version": "0.1.0",
    "name": "Set_Speed_Test_Interval",
    "group": "Sensor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "MAC",
            "description": "<p>Mac address of the sensor to have its speed test inteveral set</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>username credentials of the sensor to have its speed test inteveral set</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password credentials of the sensor to have its speed test inteveral set</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>Building sensors retrieved successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Sensor speed test interval set successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some fields are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "EmptyPort",
            "description": "<p>Port Number not configured.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Port Number not configured\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/set_speed_test_interval.php",
    "groupTitle": "Sensor"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Change Flapping Settings",
    "version": "0.1.0",
    "name": "Change_Flapping_Settings",
    "group": "Settings",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "flapping_threshold",
            "description": "<p>The threshold where sensors are considered Flapping.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>All parameters updated successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Settings updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some fields are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidEmail",
            "description": "<p>The email address is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid email address\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/change_flapping_settings.php",
    "groupTitle": "Settings"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Change Report Settings",
    "version": "0.1.0",
    "name": "Change_Report_Settings",
    "group": "Settings",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report_time",
            "description": "<p>Time of the day (24-hour format) where the Daily Report is generated and sent out.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email_recipient",
            "description": "<p>The email address of the recipient for both the notification emails and Daily Reports.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "max_data_gap",
            "description": "<p>The maximum allowed time period (in seconds) between 2 consecutive data sets from the same sensor to be not considered &quot;down&quot;.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sensor_offline_allowance",
            "description": "<p>The minimim amount of time (in minutes) a sensor must be down for before sending out notification email.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "Success",
            "description": "<p>All parameters updated successfully.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"200\",\n  \"message\": \"Settings updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingFields",
            "description": "<p>Some fields are missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidEmail",
            "description": "<p>The email address is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Missing Fields\"\n    }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    {\n\t \"status\": \"200\",\n      \"error\": \"Invalid email address\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/change_report_settings.php",
    "groupTitle": "Settings"
  },
  {
    "type": "post",
    "url": "/backend/restful-apis/:id",
    "title": "Retrieve Settings",
    "version": "0.1.0",
    "name": "Retrieve_Settings",
    "group": "Settings",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report_time",
            "description": "<p>Time of the day (24-hour format) where the Daily Report is generated and sent out.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "max_data_gap",
            "description": "<p>The maximum allowed time period (in seconds) between 2 consecutive data sets from the same sensor to be not considered &quot;down&quot;.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "flapping_threshold",
            "description": "<p>The maximum allowance &quot;downs&quot; a sensor can make (exclusive) within any 10-min time block to be considered not flapping.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "notification_recipient",
            "description": "<p>The email address of the recipient for the notification emails.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "report_recipient",
            "description": "<p>The email address of the recipient for the daily generated reports.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sensor_offline_allowance",
            "description": "<p>The minimim amount of time (in minutes) a sensor must be down for before sending out notification email.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n\t  \"daily_notification_time\": \"0000\",\n\t  \"max_data_gap\": \"20\",\n\t  \"flapping_threshold\": \"5\",\n   \t  \"notification_recipient\": \"johncurtis@gmail.com\",\n\t  \"report_recipient\": \"johncurtis@gmail.com\",\n\t  \"sensor_offline_allowance\": \"30\"\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "restful-apis/retrieve_settings.php",
    "groupTitle": "Settings"
  }
] });
