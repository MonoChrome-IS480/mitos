<?php

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

if (!isset($_POST['MAC'])) {
	$result = array(
		"error" => "empty fields"
	);
	exit(json_encode($result));
}

require "../vendor/autoload.php";

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

$up_status = $client->fluent->up_status;

$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
$timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

$entry = array(
    "MAC" => $_POST['MAC'],
    "time" => $timestamp
);

// $up_status->insertOne($entry);
$up_status->findOneAndReplace(["MAC" => $_POST['MAC']], $entry, ["upsert" => true]);


exit('{"status":"entry recorded"}');
