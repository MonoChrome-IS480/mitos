<?php

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

if (!isset($_POST['MAC']) || !isset($_POST['upload']) || !isset($_POST['download']) || !isset($_POST['interval']) || !isset($_POST['model']) || !isset($_POST['name'])
    || !isset($_POST['uptime'])  || !isset($_POST['storageTotal']) || !isset($_POST['storageUsed']) || !isset($_POST['memoryTotal'])
    || !isset($_POST['memoryUsed']) || !isset($_POST['cpuFreq']) || !isset($_POST['cpuLoad']) || !isset($_POST['processorTemp'])) {
	$result = array(
		"error" => "empty fields"
	);
	exit(json_encode($result));
}

require "../vendor/autoload.php";

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

//find the which building this sensor is attached
$sensor_matrix = $client->fluent->sensor_matrix;

$query = array(
    "MAC" => $_POST['MAC']
);

$sensor_info = $sensor_matrix->findOne($query);

if ($sensor_info == NULL) {
    exit('{"error":"no such sensor"}');
}

$building = $sensor_info['building'];

//get interval
$interval = "-";
switch ($_POST['interval']) {
    case '*/5 * * * ':
        $interval = '5';
        break;
    case '*/15 * * *':
        $interval = '15';
        break;
    case '0,30 * * *':
        $interval = '30';
        break;
    case '0 * * * * ':
        $interval = '60';
        break;
    default:
        $interval = $_POST['interval'];
        // if (isset($_POST['interval']) && $_POST['interval']!="") {
        //     $cronInterval = $_POST['interval'];
        // } else {
        //     $cronInterval = '0 * * * *';
        // }
        break;
}


//insert entry into DB
$speed_test_collection = $client->fluent->snmp_speed_test;

$curDate = new DateTime($time = "now", new DateTimeZone("Singapore"));
$timestamp = new MongoDB\BSON\UTCDateTime(($curDate->getTimestamp())*1000);

$entry = array(
    "MAC" => $_POST['MAC'],
    "building" => $building,
    "model" => $_POST['model'],
    "systemName" => $_POST['name'],
    "upload" => floatval($_POST['upload']),
    "download" => floatval($_POST['download']),
    "uptime" => intval($_POST['uptime']),
    "storageTotal" => intval($_POST['storageTotal']),
    "storageUsed" => intval($_POST['storageUsed']),
    "memoryTotal" => intval($_POST['memoryTotal']),
    "memoryUsed" => intval($_POST['memoryUsed']),
    "cpuFreq" => intval($_POST['cpuFreq']),
    "cpuLoad" => intval($_POST['cpuLoad']),
    "processorTemp" => intval($_POST['processorTemp']),
    "current_interval" => $interval,
    "time" => $timestamp
);

$speed_test_collection->insertOne($entry);
//$speed_test_collection->findOneAndReplace(["MAC" => $_POST['MAC']], $entry);

exit('{"status":"snmp and speed test results recorded"}');
