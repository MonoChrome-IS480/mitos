<?php

/**
 * @api {post} /backend/restful-apis/:id Reboot Sensor
 * @apiVersion 0.1.0
 * @apiName Reboot
 * @apiGroup Sensor
 *
 * @apiParam {String} MAC Mac address of the sensor to be rebooted
 * @apiParam {String} username username credentials of the sensor to be rebooted
 * @apiParam {String} password password credentials of the sensor to be rebooted
 *
 * @apiSuccess Success Sensor rebooted successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Sensor rebooted successfully"
 *     }
 *
 * @apiError MissingFields Some Fields are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError EmptyPort Port Number not configured.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Port Number not configured"
 *     }
 *
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');


if (!isset($_POST['MAC']) || !isset($_POST['username']) || !isset($_POST['password'])) {
    exit('{"error" : "smt\'s missing"}');
}

require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';

$portInQuestion = RPIFIORS($_POST['MAC']);

if ($portInQuestion == "") {
    exit('{"error" : "no port. cant do nothin wid this"}');
}

$data = '';

if($ssh = ssh2_connect('localhost', $portInQuestion)) {
    if(ssh2_auth_password($ssh, $_POST['username'], $_POST['password'])) {
        fclose(ssh2_exec($ssh, 'sudo reboot'));
        // $stream = ssh2_exec($ssh, 'sudo reboot');
        // stream_set_blocking($stream, true);
        // while($buffer = fread($stream, 4096)) {
        //     $data .= $buffer;
        // }
        // fclose($stream);
        $data = "reboot command sent";
    } else {
        exit('{"error" : "authentication failed"}');
    }
}

exit('{
    "success" : "command executed",
    "message" : "'.$data.'"
}');


/*
$cmd = "sudo -H -u monochrome bash -c \"sshpass -p '".$_POST['password']."' ssh ".$_POST['username']."@localhost -p ".$portInQuestion." -t 'command; sudo reboot'\"";


$commandOutput = null;

exec($cmd, $commandOutput);




exit('{
    "success" : "command executed"
}');
*/
