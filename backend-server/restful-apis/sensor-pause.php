<?php

/**
 * @api {post} /backend/restful-apis/:id Pause Sensor
 * @apiVersion 0.1.0
 * @apiName Pause
 * @apiGroup Sensor
 *
 * @apiParam {String} mac_addresses Mac Addresses of the sensors to pause
 * @apiParam {String} pause_status True to pause, false to unpause
 *
 * @apiSuccess Success Sensors paused successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Sensors paused successfully"
 *     }
 *
 * @apiError MissingMACS Mac addresses are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError InvalidMACS The mac addresses are invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid mac addresses"
 *     }
 *
 * @apiError InvalidPause The pause status is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid pause status"
 *     }
 */

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');


if (!isset($_POST['mac_addresses']) || !isset($_POST['pause_status'])) {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);
	exit(json_encode($error));
}

$mac_addresses = explode(",",$_POST['mac_addresses']);
//
// foreach ($mac_addreses as $mac) {
//     if (!preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $mac)) {
//     	$error = array(
//     		"status" => 200,
//     		"error" => "invalid mac address"
//     	);
//     	exit(json_encode($error));
//     }
// }

if (!($_POST['pause_status'] == "true" || $_POST['pause_status'] == "false")) {
    $error = array(
		"status" => 200,
		"error" => "invalid pause status"
	);
	exit(json_encode($error));
}

require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';

$pause_status = false;
if ($_POST['pause_status'] == "true") {
    $pause_status = true;
}

echo json_encode(pause_sensors($mac_addresses, $pause_status));

exit();
