<?php

/**
 * @api {post} /backend/restful-apis/:id Retrieve Settings
 * @apiVersion 0.1.0
 * @apiName Retrieve Settings
 * @apiGroup Settings
 *
 * @apiSuccess {String} report_time Time of the day (24-hour format) where the Daily Report is generated and sent out.
 * @apiSuccess {String} max_data_gap The maximum allowed time period (in seconds) between 2 consecutive data sets from the same sensor to be not considered "down".
 * @apiSuccess {String} flapping_threshold The maximum allowance "downs" a sensor can make (exclusive) within any 10-min time block to be considered not flapping.
 * @apiSuccess {String} notification_recipient The email address of the recipient for the notification emails.
 * @apiSuccess {String} report_recipient The email address of the recipient for the daily generated reports.
 * @apiSuccess {String} sensor_offline_allowance The minimim amount of time (in minutes) a sensor must be down for before sending out notification email.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *	  "daily_notification_time": "0000",
 *	  "max_data_gap": "20",
 *	  "flapping_threshold": "5",
 *    	  "notification_recipient": "johncurtis@gmail.com",
 *	  "report_recipient": "johncurtis@gmail.com",
 *	  "sensor_offline_allowance": "30"
 *	}
 *
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');


require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

$settings = $client->fluent->universal_settings->findOne(
    [],
    [
        'projection' => [
            '_id' => 0,
            'daily_notification_time' => 1,
            'max_data_gap' => 1,
            'flapping_threshold' => 1,
            'notification_recipient' => 1,
            'report_recipient' => 1,
	    'sensor_offline_allowance' => 1
        ]
    ]
);

echo json_encode($settings);

exit();
