<?php

/**
 * @apiIgnore Not in use method
 * @api {post} /backend/restful-apis/:id Get Alternate Uptime Charts
 * @apiVersion 0.1.0
 * @apiName Alternate Uptime Charts
 * @apiGroup Charts
 *
 * @apiParam {String} mac The mac address of the sensor that the chart belongs to.
 * @apiParam {String} start_date The starting period of the chart.
 * @apiParam {String} end_date The ending period of the chart.
 * @apiParam {String} interval The interval period between data points on the chart.
 *
 * @apiSuccess Success Chart data retrieved successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Chart data retrieved successfully"
 *     }
 *
 * @apiError MissingFields Some fields are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');


if (!isset($_POST['building'])) {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);

	exit(json_encode($error));
}

require '../vendor/autoload.php';
require '../api-functions/chart_data.php';

$building = strtoupper($_POST['building']);

$start_date = "2016-10-02";
if (isset($_POST['start_date'])) {
	$start_date = $_POST['start_date'];
}

$end_date = "2016-10-07";
if (isset($_POST['end_date'])) {
	$end_date = $_POST['end_date'];
}

$interval = "30";
if (isset($_POST['interval'])) {
	$interval = $_POST['interval'];
}

echo json_encode(generateByBuildingAlt($building, $start_date, $end_date, $interval));

exit();
