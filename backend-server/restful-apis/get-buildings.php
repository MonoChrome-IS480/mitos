<?php

/**
 * @api {post} /backend/restful-apis/:id Get Buildings
 * @apiVersion 0.1.0
 * @apiName Get
 * @apiGroup Buildings
 *
 * @apiSuccess Success Buildings retrieved successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Buildings retrieved successfully"
 *     }
 *
 */

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

require '../websocket-functions/common/common-functions.php';
require '../api-functions/sensor-management.php';

echo json_encode(getAllBuildings());

exit();