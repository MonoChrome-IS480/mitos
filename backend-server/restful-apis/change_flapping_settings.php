<?php

/**
 * @api {post} /backend/restful-apis/:id Change Flapping Settings
 * @apiVersion 0.1.0
 * @apiName Change Flapping Settings
 * @apiGroup Settings
 *
 * @apiParam {String} flapping_threshold The threshold where sensors are considered Flapping.
 *
 * @apiSuccess Success All parameters updated successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Settings updated successfully"
 *     }
 *
 * @apiError MissingFields Some fields are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError InvalidEmail The email address is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid email address"
 *     }
 */

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

if (!isset($_POST['flapping_threshold'])) {
    exit('{"error" : "empty fields"}');
}

if (!filter_var($_POST['flapping_threshold'], FILTER_VALIDATE_INT)) {
	$error = array(
		"status" => "200",
		"error" => "invalid threshold"
	);
	exit(json_encode($error));
}

require '../vendor/autoload.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

$settings = $client->fluent->universal_settings->findOne([]);

$settings['flapping_threshold'] = $_POST['flapping_threshold'];

$result = $client->fluent->universal_settings->updateOne(
    [],
    ['$set' => ['flapping_threshold' => $settings['flapping_threshold']]]
);

$result = array(
    'status' => '200',
    'message' => 'settings updated successfully'
);

echo json_encode($result);

exit();
