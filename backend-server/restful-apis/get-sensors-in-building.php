<?php

/**
 * @api {post} /backend/restful-apis/:id Get Building's Sensors
 * @apiVersion 0.1.0
 * @apiName Get Sensors
 * @apiGroup Buildings
 *
 * @apiParam {String} building Building name of building to have all its sensors retrieved
 *
 * @apiSuccess Success Building sensors retrieved successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Building sensors retrieved successfully"
 *     }
 *
 * @apiError MissingBuilding Building name is missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

if (!isset($_POST['building'])) {
	$result = array(
		"status" => "200",
		"error" => "empty fields"
	);
	echo json_encode($result);
	exit();
}

require "../websocket-functions/common/common-functions.php";
require '../api-functions/sensor-management.php';

echo json_encode(getSensorsInBuilding($_POST['building']));

exit();
