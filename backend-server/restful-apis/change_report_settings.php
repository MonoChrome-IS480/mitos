<?php

/**
 * @api {post} /backend/restful-apis/:id Change Report Settings
 * @apiVersion 0.1.0
 * @apiName Change Report Settings
 * @apiGroup Settings
 *
 * @apiParam {String} report_time Time of the day (24-hour format) where the Daily Report is generated and sent out.
 * @apiParam {String} email_recipient The email address of the recipient for both the notification emails and Daily Reports.
 * @apiParam {String} max_data_gap The maximum allowed time period (in seconds) between 2 consecutive data sets from the same sensor to be not considered "down".
 * @apiParam {String} sensor_offline_allowance The minimim amount of time (in minutes) a sensor must be down for before sending out notification email.
 *
 * @apiSuccess Success All parameters updated successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Settings updated successfully"
 *     }
 *
 * @apiError MissingFields Some fields are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError InvalidEmail The email address is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid email address"
 *     }
 */

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

//Checking for POST inputs and validity
if (!isset($_POST['report_time']) || !isset($_POST['email_recipient'])) {
    exit('{"error" : "empty fields"}');
}


if (!filter_var($_POST['email_recipient'], FILTER_VALIDATE_EMAIL)) {
	$error = array(
		"status" => "200",
		"error" => "invalid email address"
	);
	exit(json_encode($error));
}

$time = (int) $_POST['report_time'];

if (strlen($_POST['report_time']) != 4 || !is_numeric($_POST['report_time'])) {
	$error = array(
		"status" => "200",
		"error" => "invalid report time"
	);
	exit(json_encode($error));
}

if (!is_numeric($_POST['max_data_gap']) || $_POST['max_data_gap'] < 1) {
	$error = array(
		"status" => "200",
		"error" => "invalid data time gap between sensors"
	);
	exit(json_encode($error));
}

if (!is_numeric($_POST['sensor_offline_allowance']) || $_POST['sensor_offline_allowance'] < 1) {
	$error = array(
		"status" => "200",
		"error" => "invalid sensor offline allowance period"
	);
	exit(json_encode($error));
}

require '../vendor/autoload.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

$settings = $client->fluent->universal_settings->findOne([]);

$settings['daily_notification_time'] = $_POST['report_time'];
$settings['notification_recipient'] = $_POST['email_recipient'];
$settings['report_recipient'] = $_POST['email_recipient'];
$settings['max_data_gap'] = $_POST['max_data_gap'];
$settings['sensor_offline_allowance'] = $_POST['sensor_offline_allowance'];

//Update DB
$result = $client->fluent->universal_settings->updateOne(
    [],
    ['$set' =>
        [
            'daily_notification_time' => $settings['daily_notification_time'],
            'notification_recipient' => $settings['notification_recipient'],
            'report_recipient' => $settings['report_recipient'],
			'max_data_gap' => $settings['max_data_gap'],
			'sensor_offline_allowance' => $settings['sensor_offline_allowance'] 
        ]
    ]
);

$result = array(
    'status' => '200',
    'message' => 'settings updated successfully'
);

if (isset($_POST['report_time']) && $_POST['report_time'] != "") {
	$timing = $_POST['report_time'];

	$hour = substr($timing,0,2);
	$minute = substr($timing,2,2);

	$hour_int = (int) $hour;
	$minute_int = (int) $minute;

	//check for timing validity
	if ($hour < 0 || $hour > 23 || $minute < 0 || $minute > 59) {
		$error = array(
		"status" => "200",
		"error" => "invalid report time"
		);
		exit(json_encode($error));
	}

	$hour = $hour_int - 8;
	if ($hour < 0) { //Negative Values
		$hour = 24 - $hour;
	} 

	if ($hour < 10) {
		$hour = "0".$hour;
	}

	//Execute command
	$cronInterval = $minute.' '.$hour.' * * *';

	$cmd = "sudo sed -i 1c\\\\'".$cronInterval." root /usr/bin/php /var/www/html/backend/api-functions/reportGenerator.php >> /home/monochrome/daily_report.log' /etc/cron.d/reportGenerator"; //this needs to be changed

	exec($cmd);
}

echo json_encode($result);

exit();
