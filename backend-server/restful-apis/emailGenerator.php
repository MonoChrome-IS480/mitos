<?php
// Pear Mail Library
include_once("Mail.php");
include_once("Mail/mime.php");

require_once '/var/www/html/backend/vendor/autoload.php';
require_once '/var/www/html/backend/websocket-functions/common/common-functions.php';

$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

generateDownEmails();

function generateDownEmails() {
	global $client;

	$sensor_matrix = $client->fluent->sensor_matrix;

	$all_sensors = $sensor_matrix->find([]);

	foreach($all_sensors as $sensor){
		$mac = $sensor['MAC'];
		$status = checkStatus($mac);

		if ($status == "down"){
			sendNotificationEmail($mac);
		}
	}
}

#send notification for sensors down for > 30 mins, #to be activate when notification detects sensor down.
function sendNotificationEmail($mac_add){
	global $client;

	echo $mac_add."\n";

	#sensor data/matrix collection
	$sensor_data = $client->fluent->sensor_data;
	$sensor_data_arc = $client->fluent->sensor_data_archive;
	$sensor_matrix = $client->fluent->sensor_matrix;

	$uni_settings = $client->fluent->universal_settings;

	$email_notification = $client->fluent->email_log;

	$settings = $uni_settings->findOne([]);

	// Constructing the email
	$sender = "is480mc2016@gmail.com";                       			// Your name and email address
	$recipient = $settings['notification_recipient']; 					// The Recipients name and email address
	$text = 'This is a text message.';
	
	$downtime_slack = $settings['downtime_period'];
	$x = $downtime_slack * 60;

	//Generate Time and Email Header
	$timenow = time();
	$one_day = 24 * 60 * 60; //one day in seconds
	$offset = 8 * 60 * 60;

	#check if sensor is down > $x mins

	$time_x_min = $timenow - $x; #x mins ago

	#pull last record from DB
	$last_data_set = $sensor_data->findOne(['MAC'=>$mac_add], ['sort'=> ['time' => -1]]);

	if (count($last_data_set) == 0){ #no data in $sensor_data collection, means down for > 24 hours
		$last_data_set = $sensor_data_arc->findOne(['MAC'=>$mac_add], ['sort'=> ['time' => -1]]);
	}

	$timestamp = $last_data_set['time']; #bson timestamp

	$ts_dt = $timestamp->toDateTime();

	$ts_dt_format = $ts_dt->format('Y-m-d H:i:s');

	var_dump($ts_dt_format);

	$timestamp_check = $ts_dt->getTimestamp();

	if ($timestamp_check > $time_x_min) { #the sensor has not been down for > x mins
		echo "Sensor Not down more than x minutes\n";
		return;
	}

	#check log for occurance before
	$log = $email_notification->findOne(['mac'=>$mac_add, 'recipient'=>$recipient, 'information_ts'=>$timestamp]);

	if (count($log) != 0) {
		echo "Email has been sent before!";
		return;
	}

	$subject = "Notification Alert for ".$mac_add;   //Email Subject

	//Get all Sensors
	$sensor = $sensor_matrix->findOne(['MAC'=>$mac_add]);
	//$sensor = $sensor_arr[0];

	$building = $sensor['building'];
	$level = $sensor['sensor-location-level'];
	$id = $sensor['sensor-location-id'];

	$msg = '<html><body>
	<h2 align="center"><u>MITOS Sensor Notification Alert</u></h2>
	<br>
	
	<table align="center" width="50%">
	<tr><td><b>Sensor\'s mac address:</b></td><td>'.$mac_add.'</td></tr>
	<tr><td><b>Location:</b></td><td>'.$building.'</td></tr>
	<tr><td><b>Level:</b></td><td>'.$level.$id.'</td></tr>
	<tr><td><b>Notification reason:</b></td><td>Sensor has been down for more than '.$downtime_slack.' min(s).</td></tr>
	<tr><td><b>Last downtime:</b></td><td>'.$ts_dt_format.'</td></tr>
	</table>
	
	</div>
	<br><br>
	<center><b>This is a System Generated Email, please do not reply.</b></center>
	</body></html>';

	$crlf = "\n";
	$headers = array(
					'From'          => $sender,
					'Return-Path'   => $sender,
					'Subject'       => $subject
					);

	// Creating the Mime message
	$mime = new Mail_mime($crlf);

	// Setting the body of the email
	$mime->setTXTBody($text);
	$mime->setHTMLBody($msg);

	$body = $mime->get();
	$headers = $mime->headers($headers);

	$smtp = Mail::factory('smtp', array(
			'host' => 'ssl://smtp.gmail.com',
			'port' => '465',
			'auth' => true,
			'username' => 'is480mc2016@gmail.com',
			'password' => 'asdf0000'
		));

	$mail = $smtp->send($recipient, $headers, $body);

	if (PEAR::isError($mail)) {
		echo('<p>' . $mail->getMessage() . '</p>');
	} else {
		$sent_time = new MongoDB\BSON\UTCDateTime(time()*1000);

		$tobeInserted = array();
		$tobeInserted['recipient'] = $recipient;
		$tobeInserted['mac'] = $mac_add;
		$tobeInserted['information_ts'] = $timestamp;
		$tobeInserted['sent_time'] = $sent_time;

		$email_notification->insertOne($tobeInserted);

		echo('<p>Message successfully sent!</p>');
	}
}

exit();
?>
