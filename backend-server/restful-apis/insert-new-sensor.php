<?php

/**
 * @api {post} /backend/restful-apis/:id Insert Sensor
 * @apiVersion 0.1.0
 * @apiName Insert
 * @apiGroup Sensor
 *
 * @apiParam {String} MAC Mac address of the sensor to insert
 * @apiParam {String} geo-region Geographical Region of the sensor to insert
 * @apiParam {String} building The building name where the sensor to insert is located
 * @apiParam {String} sensor-location-level The level in the building where the sensor to insert is located
 * @apiParam {String} sensor-location-id The id of sensor to insert is within the building and level
 * @apiParam {String} dangerDisk The threshold for Danger status for the sensor in terms of disk usage
 * @apiParam {String} dangerCPU The threshold for Danger status for the sensor in terms of CPU usage
 * @apiParam {String} dangerRAM The threshold for Danger status for the sensor in terms of RAM usage
 * @apiParam {String} dangerDTPercentage The threshold for Danger status for the sensor in terms of downtime percentage
 * @apiParam {String} dangerTemp The threshold for Danger status for the sensor in terms of Temperature
 * @apiParam {String} warningDisk The threshold for Warning status for the sensor in terms of disk usage
 * @apiParam {String} warningCPU The threshold for Warning status for the sensor in terms of CPU usage
 * @apiParam {String} warningRAM The threshold for Warning status for the sensor in terms of RAM usage
 * @apiParam {String} warningDTPercentage The threshold for Warning status for the sensor in terms of downtime percentage
 * @apiParam {String} warningTemp The threshold for Warning status for the sensor in terms of Temperature
 *
 * @apiSuccess Success Sensor inserted successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Sensor inserted successfully"
 *     }
 *
 * @apiError MissingFields Some of the fields are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError InvalidMAC The mac address is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid mac address"
 *     }
 *
 * @apiError InvalidDisk The Disk threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid disk usage threshold"
 *     }
 *
 * @apiError InvalidCPU The CPU threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid CPU usage threshold"
 *     }
 *
 * @apiError InvalidRAM The RAM threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid RAM usage threshold"
 *     }
 *
 * @apiError InvalidCPU The Downtime Percentage threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid Downtime percentage threshold"
 *     }
 *
 * @apiError InvalidTemp The temperature threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid temperature threshold"
 *     }
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

$thresholds = array(
    'danger' => array(
        'disk_usage' => 0.9,
        'cpu_usage' => 60,
        'ram_usage' => 0.6,
        'downtime_percentage' => 0.6,
        'temperature' => 65,
    ),
    'warning' => array(
        'disk_usage' => 0.6,
        'cpu_usage' => 40,
        'ram_usage' => 0.4,
        'downtime_percentage' => 0.4,
        'temperature' => 60,
    )
);

if (!isset($_POST['MAC']) || !isset($_POST['geo-region']) || !isset($_POST['building']) || !isset($_POST['sensor-location-level']) ||
    !isset($_POST['sensor-location-id'])) {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);

	exit(json_encode($error));
}

if ($_POST['geo-region']=="" || $_POST['building']=="" || $_POST['sensor-location-level']=="" || $_POST['sensor-location-id']=="") {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);

	exit(json_encode($error));
}

if (!preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $_POST['MAC'])) {
	$error = array(
		"status" => 200,
		"error" => "invalid mac address"
	);
	exit(json_encode($error));
}

require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';


$mac_address = strtoupper($_POST['MAC']);
$geo_region = strtoupper($_POST['geo-region']);
$building = strtoupper($_POST['building']);
$sensor_location_level = strtoupper($_POST['sensor-location-level']);
$sensor_location_id = strtoupper($_POST['sensor-location-id']);
$port = "";
if (isset($_POST['port'])) {
	$port = $_POST['port'];
}

//danger thresholds
if (isset($_POST['dangerDisk'])) {
    if (filter_var($_POST['dangerDisk'], FILTER_VALIDATE_FLOAT) && $_POST['dangerDisk'] > 0 && $_POST['dangerDisk'] < 1) {
        $thresholds['danger']['disk_usage'] = $_POST['dangerDisk'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid disk usage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['dangerCPU'])) {
    if (filter_var($_POST['dangerCPU'], FILTER_VALIDATE_FLOAT) && $_POST['dangerCPU'] > 0 && $_POST['dangerCPU'] < 100) {
        $thresholds['danger']['cpu_usage'] = $_POST['dangerCPU'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid cpu usage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['dangerRAM'])) {
    if (filter_var($_POST['dangerRAM'], FILTER_VALIDATE_FLOAT) && $_POST['dangerRAM'] > 0 && $_POST['dangerRAM'] < 1) {
        $thresholds['danger']['ram_usage'] = $_POST['dangerRAM'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid ram usage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['dangerDTPercentage'])) {
    if (filter_var($_POST['dangerDTPercentage'], FILTER_VALIDATE_FLOAT) && $_POST['dangerDTPercentage'] > 0 && $_POST['dangerDTPercentage'] < 1) {
        $thresholds['danger']['downtime_percentage'] = $_POST['dangerDTPercentage'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid downtime percentage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['dangerTemp'])) {
    if (filter_var($_POST['dangerTemp'], FILTER_VALIDATE_FLOAT) && $_POST['dangerTemp'] > 0 && $_POST['dangerTemp'] < 150) {
        $thresholds['danger']['temperature'] = $_POST['dangerTemp'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid temperature"
        );
        exit(json_encode($error));
    }
}

//warning thresholds
if (isset($_POST['warningDisk'])) {
    if (filter_var($_POST['warningDisk'], FILTER_VALIDATE_FLOAT) && $_POST['warningDisk'] > 0 && $_POST['warningDisk'] < 1 && $_POST['warningDisk'] < $thresholds['danger']['disk_usage']) {
        $thresholds['warning']['disk_usage'] = $_POST['warningDisk'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid disk usage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['warningCPU'])) {
    if (filter_var($_POST['warningCPU'], FILTER_VALIDATE_FLOAT) && $_POST['warningCPU'] > 0 && $_POST['warningCPU'] < 100 && $_POST['warningCPU'] < $thresholds['danger']['cpu_usage']) {
        $thresholds['warning']['cpu_usage'] = $_POST['warningCPU'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid cpu usage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['warningRAM'])) {
    if (filter_var($_POST['warningRAM'], FILTER_VALIDATE_FLOAT) && $_POST['warningRAM'] > 0 && $_POST['warningRAM'] < 1 && $_POST['warningRAM'] < $thresholds['danger']['ram_usage']) {
        $thresholds['warning']['ram_usage'] = $_POST['warningRAM'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid ram usage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['warningDTPercentage'])) {
    if (filter_var($_POST['warningDTPercentage'], FILTER_VALIDATE_FLOAT) && $_POST['warningDTPercentage'] > 0 && $_POST['warningDTPercentage'] < 1 && $_POST['warningDTPercentage'] < $thresholds['danger']['downtime_percentage']) {
        $thresholds['warning']['downtime_percentage'] = $_POST['warningDTPercentage'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid downtime percentage"
        );
        exit(json_encode($error));
    }
}
if (isset($_POST['warningTemp'])) {
    if (filter_var($_POST['warningTemp'], FILTER_VALIDATE_FLOAT) && $_POST['warningTemp'] > 0 && $_POST['warningTemp'] < 150 && $_POST['warningTemp'] < $thresholds['danger']['temperature']) {
        $thresholds['warning']['temperature'] = $_POST['warningTemp'];
    } else {
        $error = array(
            "status" => 200,
            "error" => "invalid temperature"
        );
        exit(json_encode($error));
    }
}


echo InsertNewSensor($mac_address, $geo_region, $building, $sensor_location_level, $sensor_location_id, "", $port, $thresholds);

exit();
