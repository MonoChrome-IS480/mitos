<?php

/**
 * @api {post} /backend/restful-apis/:id Pin Sensor
 * @apiVersion 0.1.0
 * @apiName Pause
 * @apiGroup Sensor
 *
 * @apiParam {String} mac_addresses Mac Addresses of the sensors to pin
 * @apiParam {String} pause_status True to pin, false to unpin
 *
 * @apiSuccess Success Sensors pinned/unpinned successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Sensors pin/unpinned successfully"
 *     }
 *
 * @apiError MissingMACS Mac addresses are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError InvalidMACS The mac addresses are invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid mac addresses"
 *     }
 *
 * @apiError InvalidPin The pin status is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid pin status"
 *     }
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');


if (!isset($_POST['MAC']) || !isset($_POST['pin_status'])) {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);
	exit(json_encode($error));
}

if (!preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $_POST['MAC'])) {
	$error = array(
		"status" => 200,
		"error" => "invalid mac address"
	);
	exit(json_encode($error));
}

if (!($_POST['pin_status'] == "true" || $_POST['pin_status'] == "false")) {
    $error = array(
		"status" => 200,
		"error" => "invalid pin status"
	);
	exit(json_encode($error));
}

require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';


$mac_address = $_POST['MAC'];
$pin_status = false;
if ($_POST['pin_status'] == "true") {
    $pin_status = true;
}

// echo "sending";

echo watchlistPin($mac_address, $pin_status);

exit();
