<?php

/**
 * @api {post} /backend/restful-apis/:id Get Notifications Log
 * @apiVersion 0.1.0
 * @apiName Notifications Log
 * @apiGroup Notifications
 *
 * @apiParam {String} log_count The number of past notifications to generate.
 *
 * @apiSuccess Success Notifcations Log retrieved successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Notifcations Log retrieved successfully"
 *     }
 *
 */

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

require '../vendor/autoload.php';
include_once('../websocket-functions/common/common-functions.php');


function getAllNotifications($number_of_records=100){
	$client = new MongoDB\Client("mongodb://127.0.0.1:27017");

	$notifications_cursor = $client->fluent->notifications;

	$endResult = array();

	//limit to $number_of_records records, in desc order by timestamp
	$results = $notifications_cursor->find([], ['limit' => $number_of_records, 'sort' => ['timestamp' => -1]])->toArray();
	

	foreach ($results as $result) {
		$timestamp = $result['timestamp']->toDateTime()->setTimezone(new DateTimeZone("Singapore"));
		$entry = array(
            "mac" => $result['mac'],
			"building" => $result["building"],
			"level" => $result["level"],
			"id" => $result["id"],
    		"problem" => $result['problem'],
    		"timestamp" => $timestamp
        );

        array_push($endResult, $entry);

	}

	// $endResult['data'] = $return;

	return $endResult;
}

$numberOfEntries = 100;
if (isset($_POST["log_count"])) {
    $numberOfEntries = intval($_POST["log_count"]);
}

echo json_encode(getAllNotifications($numberOfEntries));

exit();
