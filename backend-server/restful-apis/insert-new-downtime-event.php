<?php

/**
 * @api {post} /backend/restful-apis/:id Reboot Sensor
 * @apiVersion 0.1.0
 * @apiName Reboot
 * @apiGroup Sensor
 *
 * @apiParam {String} mac_addresses Mac Address of the sensors to be schedule downtime
 * @apiParam {String} type Type of downtime to be scheduled
 * @apiParam {String} startDate Start Date of the scheduled downtime
 * @apiParam {String} endDate End Date of the scheduled downtime
 *
 * @apiSuccess Success Downtime Schedules implemented successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Downtime Schedules implemented successfully"
 *     }
 *
 * @apiError MissingFields Some fields are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing fields"
 *     }
 *
 * @apiError InvalidMACS Some Mac Address provided are invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid Mac Address(es)"
 *     }
 *
 * @apiError InvalidType Type provided invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid Type"
 *     }
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

if (!isset($_POST['mac_addresses']) || !isset($_POST['type']) || !isset($_POST['startDate']) || !isset($_POST['endDate'])) {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);
	exit(json_encode($error));
}

//validate
$mac_addresses = explode(';',$_POST['mac_addresses']);

foreach ($mac_addresses as $mac) {
    if (!preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $mac)) {
    	$error = array(
    		"status" => 200,
    		"error" => "invalid mac address"
    	);
    	exit(json_encode($error));
    }
}

$type = strtoupper($_POST['type']);
if (!($type == "DAILY" || $type == "WEEKLY")) {
    $error = array(
        "status" => 200,
        "error" => "invalid type"
    );
    exit(json_encode($error));
}

$startDate = date('m/d/y', strtotime($_POST['startDate']));
$endDate = date('m/d/y', strtotime($_POST['endDate']));

if ($type == "DAILY") {
    if ($startDate == "01/01/70" || $endDate == "01/01/70") {
        $error = array(
            "status" => 200,
            "error" => "invalid date"
        );
        exit(json_encode($error));
    }
} else {
    $startArr = explode("|",$_POST['startDate']);
    $endArr = explode("|",$_POST['endDate']);
    if (!ctype_digit($startArr[0]) || !ctype_digit($endArr[0]) || strtotime($startArr[1]) == "01/01/70" || strtotime($endArr[1]) == "01/01/70") {
        $error = array(
            "status" => 200,
            "error" => "invalid date"
        );
        exit(json_encode($error));
    }
}



require '../vendor/autoload.php';
require '../api-functions/downtime-schedule-management.php';

echo json_encode(insertNewDowntimeEvent($mac_addresses, $type, $_POST['startDate'], $_POST['endDate']));

exit();
