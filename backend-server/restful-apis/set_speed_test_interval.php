<?php

/**
 * @api {post} /backend/restful-apis/:id Set Sensor Speed Test Interval
 * @apiVersion 0.1.0
 * @apiName Set Speed Test Interval
 * @apiGroup Sensor
 *
 * @apiParam {String} MAC Mac address of the sensor to have its speed test inteveral set
 * @apiParam {String} username username credentials of the sensor to have its speed test inteveral set
 * @apiParam {String} password password credentials of the sensor to have its speed test inteveral set
 *
 * @apiSuccess Success Building sensors retrieved successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Sensor speed test interval set successfully"
 *     }
 *
 * @apiError MissingFields Some fields are missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError EmptyPort Port Number not configured.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Port Number not configured"
 *     }
 *
 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

if (!isset($_POST['MAC']) || !isset($_POST['username']) || !isset($_POST['password'])) {
    exit('{"error" : "smt\'s missing"}');
}

require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';

$portInQuestion = RPIFIORS($_POST['MAC']);

if ($portInQuestion == "") {
    exit('{"error" : "no port. cant do nothin wid this"}');
}

$data = '';
$cronInterval = '';

switch ($_POST['interval']) {
    case '5':
        $cronInterval = '*/5 * * * *';
        break;
    case '15':
        $cronInterval = '*/15 * * * *';
        break;
    case '30':
        $cronInterval = '0,30 * * * *';
        break;
    case '60':
        $cronInterval = '0 * * * *';
        break;
    default:
        // $cronInterval = '0 * * * *';
        if (isset($_POST['interval']) && $_POST['interval']!="") {
            $intervalArr = explode(" ", $_POST['interval']);
            var_dump(count($intervalArr));
            if (count($intervalArr) >= 5) {
                $cronInterval = $_POST['interval'];
            } else {
                exit('{"error" : "invalid interval"}');
            }
        } else {
            $cronInterval = '0 * * * *';
        }
        break;
}

if($ssh = ssh2_connect('localhost', $portInQuestion)) {
    if(ssh2_auth_password($ssh, $_POST['username'], $_POST['password'])) {
        $cmd = "sudo sed -i '/'snmp-speedtest.sh'/c\\".$cronInterval." root bash /home/pi/snmp-speedtest.sh' /etc/cron.d/snmp-speedtest";

        fclose(ssh2_exec($ssh, $cmd));

        $data = "interval adjusted";
    } else {
        exit('{"error" : "authentication failed"}');
    }
}

exit('{
    "success" : "command executed",
    "message" : "'.$data.'"
}');
