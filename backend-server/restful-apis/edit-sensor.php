<?php

/**
 * @api {post} /backend/restful-apis/:id Edit Sensor
 * @apiVersion 0.1.0
 * @apiName Edit
 * @apiGroup Sensor
 *
 * @apiParam {String} MAC Mac address of the sensor to update
 * @apiParam {String} geo-region Geographical Region of the sensor to update
 * @apiParam {String} building The building name where the sensor to update is located
 * @apiParam {String} sensor-location-level The level in the building where the sensor to update is located
 * @apiParam {String} sensor-location-id The id of sensor to update is within the building and level
 * @apiParam {String} dangerDisk The threshold for Danger status for the sensor in terms of disk usage
 * @apiParam {String} dangerCPU The threshold for Danger status for the sensor in terms of CPU usage
 * @apiParam {String} dangerRAM The threshold for Danger status for the sensor in terms of RAM usage
 * @apiParam {String} dangerDTPercentage The threshold for Danger status for the sensor in terms of downtime percentage
 * @apiParam {String} dangerTemp The threshold for Danger status for the sensor in terms of Temperature
 * @apiParam {String} warningDisk The threshold for Warning status for the sensor in terms of disk usage
 * @apiParam {String} warningCPU The threshold for Warning status for the sensor in terms of CPU usage
 * @apiParam {String} warningRAM The threshold for Warning status for the sensor in terms of RAM usage
 * @apiParam {String} warningDTPercentage The threshold for Warning status for the sensor in terms of downtime percentage
 * @apiParam {String} warningTemp The threshold for Warning status for the sensor in terms of Temperature
 *
 * @apiSuccess Success Sensor updated successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Sensor updated successfully"
 *     }
 *
 * @apiError MissingMAC Mac address is missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError InvalidMAC The mac address is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid mac address"
 *     }
 *
* @apiError InvalidDisk The Disk threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid disk usage threshold"
 *     }
 *
 * @apiError InvalidCPU The CPU threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid CPU usage threshold"
 *     }
 *
 * @apiError InvalidRAM The RAM threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid RAM usage threshold"
 *     }
 *
 * @apiError InvalidCPU The Downtime Percentage threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid Downtime percentage threshold"
 *     }
 *
 * @apiError InvalidTemp The temperature threshold is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid temperature threshold"
 *     }

 */


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');

$thresholds = array(
    'danger' => array(
        'disk_usage' => 0.9,
        'cpu_usage' => 60,
        'ram_usage' => 0.6,
        'downtime_percentage' => 0.6,
        'temperature' => 65,
    ),
    'warning' => array(
        'disk_usage' => 0.6,
        'cpu_usage' => 40,
        'ram_usage' => 0.4,
        'downtime_percentage' => 0.4,
        'temperature' => 60,
    )
);

if (!isset($_POST['MAC']) || !isset($_POST['geo-region']) || !isset($_POST['building']) || !isset($_POST['sensor-location-level']) ||
    !isset($_POST['sensor-location-id'])) {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);

	exit(json_encode($error));
}

if ($_POST['geo-region']=="" || $_POST['building']=="" || $_POST['sensor-location-level']=="" || $_POST['sensor-location-id']=="") {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);

	exit(json_encode($error));
}

if (!preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $_POST['MAC'])) {
	$error = array(
		"status" => 200,
		"error" => "invalid mac address"
	);
	exit(json_encode($error));
}

require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';


$mac_address = strtoupper($_POST['MAC']);
$geo_region = strtoupper($_POST['geo-region']);
$building = strtoupper($_POST['building']);
$sensor_location_level = strtoupper($_POST['sensor-location-level']);
$sensor_location_id = strtoupper($_POST['sensor-location-id']);
$port = "";
if (isset($_POST['port'])) {
	$port = $_POST['port'];
}

//danger thresholds
if (isset($_POST['dangerDisk']) && filter_var($_POST['dangerDisk'], FILTER_VALIDATE_FLOAT) && $_POST['dangerDisk'] > 0) {
    $thresholds['danger']['disk_usage'] = $_POST['dangerDisk'];
}
if (isset($_POST['dangerCPU']) && filter_var($_POST['dangerCPU'], FILTER_VALIDATE_FLOAT) && $_POST['dangerCPU'] > 0) {
    $thresholds['danger']['cpu_usage'] = $_POST['dangerCPU'];
}
if (isset($_POST['dangerRAM']) && filter_var($_POST['dangerRAM'], FILTER_VALIDATE_FLOAT) && $_POST['dangerRAM'] > 0) {
    $thresholds['danger']['ram_usage'] = $_POST['dangerRAM'];
}
if (isset($_POST['dangerDTPercentage']) && filter_var($_POST['dangerDTPercentage'], FILTER_VALIDATE_FLOAT) && $_POST['dangerDTPercentage'] > 0) {
    $thresholds['danger']['downtime_percentage'] = $_POST['dangerDTPercentage'];
}
if (isset($_POST['dangerTemp']) && filter_var($_POST['dangerTemp'], FILTER_VALIDATE_FLOAT) && $_POST['dangerTemp'] > 0) {
    $thresholds['danger']['temperature'] = $_POST['dangerTemp'];
}

//warning thresholds
if (isset($_POST['warningDisk']) && filter_var($_POST['warningDisk'], FILTER_VALIDATE_FLOAT) && $_POST['warningDisk'] > 0) {
    $thresholds['warning']['disk_usage'] = $_POST['warningDisk'];
}
if (isset($_POST['warningCPU']) && filter_var($_POST['warningCPU'], FILTER_VALIDATE_FLOAT) && $_POST['warningCPU'] > 0) {
    $thresholds['warning']['cpu_usage'] = $_POST['warningCPU'];
}
if (isset($_POST['warningRAM']) && filter_var($_POST['warningRAM'], FILTER_VALIDATE_FLOAT) && $_POST['warningRAM'] > 0) {
    $thresholds['warning']['ram_usage'] = $_POST['warningRAM'];
}
if (isset($_POST['warningDTPercentage']) && filter_var($_POST['warningDTPercentage'], FILTER_VALIDATE_FLOAT) && $_POST['warningDTPercentage'] > 0) {
    $thresholds['warning']['downtime_percentage'] = $_POST['warningDTPercentage'];
}
if (isset($_POST['warningTemp']) && filter_var($_POST['warningTemp'], FILTER_VALIDATE_FLOAT) && $_POST['warningTemp'] > 0) {
    $thresholds['warning']['temperature'] = $_POST['warningTemp'];
}


echo UpdateNewSensor($mac_address, $geo_region, $building, $sensor_location_level, $sensor_location_id, "", $port,  $thresholds);

exit();
