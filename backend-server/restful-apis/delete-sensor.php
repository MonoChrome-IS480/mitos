<?php

/**
 * @api {post} /backend/restful-apis/:id Delete Sensor
 * @apiVersion 0.1.0
 * @apiName Delete
 * @apiGroup Sensor
 *
 * @apiParam {String} MAC Mac address of the sensor to delete
 *
 * @apiSuccess Success Sensor deleted successfully.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "200",
 *       "message": "Sensor deleted successfully"
 *     }
 *
 * @apiError MissingMAC Mac address is missing.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Missing Fields"
 *     }
 *
 * @apiError InvalidMAC The mac address is invalid.
 *
 * @apiErrorExample Error-Response:
 *     {
 *	 "status": "200",
 *       "error": "Invalid mac address"
 *     }
 */

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token');


if (!isset($_POST['MAC'])) {
	$error = array(
		"status" => 200,
		"error" => "empty fields"
	);
	exit(json_encode($error));
}

if (!preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $_POST['MAC'])) {
	$error = array(
		"status" => 200,
		"error" => "invalid mac address"
	);
	exit(json_encode($error));
}

require '../vendor/autoload.php';
require '../api-functions/sensor-management.php';


$mac_address = strtoupper($_POST['MAC']);

echo deleteExistingSensor($mac_address);

exit();
