<?php

require '../../vendor/autoload.php';

$loop = React\EventLoop\Factory::create();

$context = new React\ZMQ\Context($loop);

$push = $context->getSocket(ZMQ::SOCKET_PUSH);
$push->connect('tcp://127.0.0.1:5555');

$loop->addPeriodicTimer(5, function () use (&$i, $push) {

    //multi-curl variables
    $mh = curl_multi_init();
    $handles = array();
    $URLs = array(
        "http://localhost/backend/websocket-functions/BFG-table-all-sensor-data/all-sensor-data-function.php",
        "http://localhost/backend/websocket-functions/status-by-region-mall/health-overview.php",
        "http://localhost/backend/websocket-functions/notifications/notifications.php",
        "http://localhost/backend/websocket-functions/overall-status/overallstatus-function.php"
        // "http://localhost/backend/websocket-functions/status-by-region-mall/server-overview.php"
    );
    $dataClass = array(
        "BFG",
        "overview",
        "notifications",
        "overall",
        // "serverOverview"
    );

    // create several requests
    for ($i = 0; $i < count($URLs); $i++) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $URLs[$i]);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);

        curl_multi_add_handle($mh, $ch);
        $handles[] = $ch;
    }

    // execute requests and poll periodically until all have completed
    $isRunning = null;
    do {
        curl_multi_exec($mh, $isRunning);
        usleep(250000);
    } while ($isRunning > 0);

    // fetch output of each request
    $outputs = array();
    for ($i = 0; $i < count($handles); $i++) {
        $outputs[$dataClass[$i]] = json_decode(trim(curl_multi_getcontent($handles[$i])));
        curl_multi_remove_handle($mh, $handles[$i]);
    }

    curl_multi_close($mh);

    $push->send(json_encode($outputs));

    //populate sensor data
    // exec("cd /var/www/html/backend/websocket-functions/ && /usr/bin/php populateSensorStatus.php  > /dev/null 2>/dev/null &");

});

$loop->run();
