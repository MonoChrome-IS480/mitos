<?php

require '../../vendor/autoload.php';

include 'Pusher.php';

$loop   = React\EventLoop\Factory::create();
$pusher = new MyApp\Pusher;

// Listen for the web server to make a ZeroMQ push after an ajax request
$context = new React\ZMQ\Context($loop);
$pull = $context->getSocket(ZMQ::SOCKET_PULL);
$pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
$pull->on('message', array($pusher, 'onHit'));

$pull->on('error', function ($e) {
    var_dump($e->getMessage());
});

$pull->on('message', function ($msg) {
    echo "$msg\n";
});


// Set up our WebSocket server for clients wanting real-time updates
$webSock = new React\Socket\Server($loop);
$webSock->listen(9000, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
$webServer = new Ratchet\Server\IoServer(
    new Ratchet\Http\HttpServer(
        new Ratchet\WebSocket\WsServer(
            new Ratchet\Wamp\WampServer(
                $pusher
            )
        )
    ),
    $webSock
);


// =================================== TEST ORIGIN LOCK ===============================
// $webSock = new React\Socket\Server($loop);
// $webSock->listen(9000, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
//
// $leApp = new Ratchet\WebSocket\WsServer(
//     new Ratchet\Wamp\WampServer(
//         $pusher
//     )
// );
//
// $checkedApp = new Ratchet\Http\OriginCheck($leApp);
// $checkedApp->allowedOrigins[] = 'opsdev.sence.io';
//
// $webServer = new Ratchet\Server\IoServer(
//     new Ratchet\Http\HttpServer($checkedApp),
//     $webSock
// );
// =================================== TEST ORIGIN LOCK ===============================

// $loop   = React\EventLoop\Factory::create();
$loop->run();
